﻿#include "DllHeader.h"
#include "Pi_i.h"


DLLEXPORT void plecsSetSizes(struct SimulationSizes* aSizes)
{
	aSizes->numInputs = 2;
	aSizes->numOutputs = 1;
	aSizes->numStates = 0;
	aSizes->numParameters = 0; //number of user parameters passed in
}

struct PI_STRUCT IL_1;

//This function is automatically called at the beginning of the simulation -> init in PLECS
DLLEXPORT void plecsStart(struct SimulationState* aState)
{
	// The current controller values were selected based on calculations using the modular criterion of optimum.
	// The modular criterion of optimum assumes a linear system without limits. The current system is limited by saturation and is asymmetrical.

	Pi_Init(&IL_1, 0.00001, 0.035, 200, 0, 1, 6); // Values tuned in simulation for a linear system running at 1.5 times the sampling time
	// Pi_Init(&IL_1, 0.00001, 0.03025, 200, 0, 1, 1); // Values tuned in simulation for a discrete shift of 2 times the sampling time
}


//This function is automatically called every sample time
//output is written to DLL output port after the output delay
DLLEXPORT void plecsOutput(struct SimulationState* aState)
{
	Pi_Calc(&IL_1, aState->inputs[0], aState->inputs[1]);
	aState->outputs[0] = IL_1.out_limit;
}
