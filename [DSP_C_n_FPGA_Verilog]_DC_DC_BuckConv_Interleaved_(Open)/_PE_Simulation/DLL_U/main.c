#include "DllHeader.h"
#include "Pi_u.h"

DLLEXPORT void plecsSetSizes(struct SimulationSizes* aSizes)
{
   aSizes->numInputs = 2;
   aSizes->numOutputs = 1;
   aSizes->numStates = 0;
   aSizes->numParameters = 0; // Number of user parameters passed in
}

struct PI_STRUCT PI_U;

// This function is automatically called at the beginning of the simulation -> init in PLECS
DLLEXPORT void plecsStart(struct SimulationState* aState)
{
   // The current controller values were selected based on calculations using the modular criterion of optimum. 
   // The modular criterion of optimum assumes a linear system without limits. The current system is limited by saturation and is asymmetrical.

    Pi_Init(&PI_U, 0.00001, 8, 200, 0, 6*10, 1.0); // where 6*10 => number of branches * max current on a single branch
    // Values were tuned in simulation for a linear [model running at] 1.5 times the sampling time
}

// This function is automatically called every sample time
// output is written to DLL output port after the output delay
DLLEXPORT void plecsOutput(struct SimulationState* aState)
{
    Pi_Calc(&PI_U, aState->inputs[0], aState->inputs[1]);
    aState->outputs[0] = PI_U.out_limit;
}

