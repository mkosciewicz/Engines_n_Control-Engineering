## Hardware Connections

Make sure your MCP2515 module is correctly connected to the Arduino:

- CS to Pin 10
- SO to Pin 12 (MISO)
- SI to Pin 11 (MOSI)
- SCK to Pin 13
- INT to Pin 2

## Powering the MCP2515

Ensure that the MCP2515 module is properly powered.