
# CAN Communication Wiring Guide

This guide summarizes the wiring required for CAN communication between an Arduino with an MCP2515 CAN controller & TJA1050 CAN transceiver, and a TMS320F2837xD microcontroller with a CAN transceiver.

## Arduino to MCP2515 Wiring

| MCP2515 Pin | Arduino Pin |
|-------------|-------------|
| VCC         | 5V          |
| GND         | GND         |
| CS          | 10          |
| SO          | MISO (12)   |
| SI          | MOSI (11)   |
| SCK         | SCK (13)    |
| INT         | 2           |

## MCP2515 to TJA1050 Wiring

| MCP2515 Pin | TJA1050 Pin |
|-------------|-------------|
| TXD         | TXD         |
| RXD         | RXD         |
| VCC         | VCC         |
| GND         | GND         |

## TJA1050 to CAN Bus Wiring

| TJA1050 Pin | CAN Network |
|-------------|-------------|
| CANH        | CAN High    |
| CANL        | CAN Low     |

- Termination resistors (120 Ohms) should be placed between CANH and CANL at both ends of the CAN network.

## TMS320F2837xD to CAN Transceiver Wiring

| TMS320F2837xD Pin | CAN Transceiver Pin |
|-------------------|---------------------|
| TXD (CAN Transmit)  | TXD                |
| RXD (CAN Receive)   | RXD                |
| VCC                 | VCC (3.3V or 5V)   |
| GND                 | GND                |

- The TMS320F2837xD should also be connected to the CAN transceiver that is compatible with its logic levels.
- Ensure all devices share a common ground.

- PULL_UP Resistors 10 [kΩ] for Tx n Rx

Make sure to verify the voltage levels required by your specific CAN transceiver, and ensure that they match the logic levels used by your microcontroller.

