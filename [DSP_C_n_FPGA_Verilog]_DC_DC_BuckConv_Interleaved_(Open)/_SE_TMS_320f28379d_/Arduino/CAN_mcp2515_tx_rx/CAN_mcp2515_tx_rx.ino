#include "Arduino.h" 
#include "AA_MCP2515.h"

// Configuration for CAN bitrate and pin assignments
const CANBitrate::Config CAN_BITRATE = CANBitrate::Config_8MHz_500kbps;
const uint8_t CAN_PIN_CS = 10;
const int8_t CAN_PIN_INT = 2;

CANConfig config(CAN_BITRATE, CAN_PIN_CS, CAN_PIN_INT);
CANController CAN(config);

void setup() {
  Serial.begin(115200);
  while (!Serial); // Ensure Serial is ready, important for boards with native USB

  while (CAN.begin(CANController::Mode::Normal) != CANController::OK) {
    Serial.println("CAN begin FAIL - delaying for 1 second");
    delay(1000);
  }
  Serial.println("CAN begin OK");

  // Setup interrupt callback for receiving CAN frames
  CAN.setInterruptCallbacks([](CANController&, CANFrame frame){
    frame.print("RX");
  }, [](CANController& controller){
    controller.setMode(CANController::Mode::Normal);
  });
}

void loop() {
  if (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n');
    if (input.length() > 0) {
      sendCANMessage(input);
    }
  }
}

void sendCANMessage(String input) {
  int firstSpace = input.indexOf(' ');
  if (firstSpace == -1) {
    Serial.println("Invalid input format. Expected \"ID DATA\".");
    return;
  }

  String idStr = input.substring(0, firstSpace);
  String dataStr = input.substring(firstSpace + 1);

  uint16_t id = idStr.toInt(); // Parses the ID as an integer
  uint8_t data[8];
  int dataIndex = 0;

  // Parsing the data as hexadecimal values
  while (dataStr.length() > 0 && dataIndex < 8) {
    int nextSpace = dataStr.indexOf(' ');
    String byteStr = dataStr;
    if (nextSpace != -1) {
      byteStr = dataStr.substring(0, nextSpace);
      dataStr = dataStr.substring(nextSpace + 1);
    } else {
      dataStr = "";
    }
    data[dataIndex++] = (uint8_t) strtol(byteStr.c_str(), NULL, 16);
  }

  Serial.print("Sending CAN message with ID: ");
  Serial.print(id, HEX);
  Serial.print(" Data: ");
  for (int i = 0; i < dataIndex; i++) {
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.println();

  CANFrame frame(id, data, dataIndex);
  if (CAN.write(frame) == CANController::IOResult::OK) {
    frame.print("TX");
  } else {
    Serial.println("CAN write FAIL");
  }
}

