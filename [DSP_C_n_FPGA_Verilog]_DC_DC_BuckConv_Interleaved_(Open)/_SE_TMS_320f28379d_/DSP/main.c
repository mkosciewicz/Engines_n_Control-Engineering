#include "F28x_Project.h"

#include "hdrs/initReglsPI.h"
#include "hdrs/reglsPI.h"

#include "hdrs/setupISR.h"
#include "hdrs/handleISR.h"

#include "hdrs/setupADC.h"
#include "hdrs/setupGPIO.h"
#include "hdrs/setupPWM.h"
#include "hdrs/setupTIMER.h"

#include "hdrs/setupCAN.h"
#include "hdrs/dataCAN.h"


unsigned int mainLoopCounter = 0;

int main(void) {
    InitSysCtrl();  // Initialize system control

    DINT;
    InitPieCtrl();  // Initialize the PIE control registers
    IER = 0x0000;
    IFR = 0x0000;

    EALLOW;
    CpuSysRegs.PCLKCR0.bit.CPUTIMER0 = 1;
    EDIS;

    InitPieVectTable();

    // Assign ISRs into the PIE vector table
    enableISR();

    // Initialize peripherals
    setupGPIO();
    setupTIMER();
    setupPWM();
    setupADC();

    initReglsPI();

    // Initialize CAN network
    setupCAN();
    mailboxingCAN();

    // Exit CAN initialization mode
    CanbRegs.CAN_CTL.bit.Init = 0;

    // Enable global interrupts
    setupISR();

    // Main control loop
    while (1) {
        mainLoopCounter++;
        handleCANTx();
        handleCANRx();
    }
}
