#include "F28x_Project.h"
#include "hdrs/handleISR.h"


// ================== SO_Global_Variables ==================
volatile bool can_tx_flag, can_rx_flag = false;
float adc_Uin_Results, adc_Uout_Results, adc_phs1_Results, adc_phs2_Results, adc_phs3_Results, adc_phs4_Results, adc_phs5_Results, adc_phs6_Results;
float i_ref = 0.5;
unsigned long long int cntr[8];

CAN_DATA_STRUCT dataADC;
// ================== EO_Global_Variables ==================

void enableISR(){
    EALLOW;
    
    PieVectTable.TIMER0_INT = &TxTimerISR;
    PieVectTable.TIMER1_INT = &RxTimerISR;

    PieVectTable.ADCA1_INT = &adc_Uin_isr; //function for ADCA interrupt 1
    PieVectTable.ADCA2_INT = &adc_Uout_isr;

    PieVectTable.ADCD2_INT = &adc_phs_1_isr;
    PieVectTable.ADCD3_INT = &adc_phs_2_isr;
    PieVectTable.ADCA4_INT = &adc_phs_3_isr;

    PieVectTable.ADCC2_INT = &adc_phs_4_isr;
    PieVectTable.ADCC3_INT = &adc_phs_5_isr;
    PieVectTable.ADCC4_INT = &adc_phs_6_isr;
    EDIS;
}

// ================== SO_ISR_Defines ========================

// *** CAN_ISR ***
__interrupt void TxTimerISR(){
    // Set the flag
    can_tx_flag = true;
    PieCtrlRegs.PIEACK.bit.ACK1=1;
    // Eventual: PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

}

__interrupt void RxTimerISR() {
    can_rx_flag = true;
    PieCtrlRegs.PIEACK.bit.ACK1 = 1;
}

// #ifndef PLECS_DLL
// TMS microcontroller specific structures for PI controllers
struct PI_STRUCT PI_U;
struct PI_STRUCT PI_IL_1;
struct PI_STRUCT PI_IL_2;
struct PI_STRUCT PI_IL_3;
struct PI_STRUCT PI_IL_4;
struct PI_STRUCT PI_IL_5;
struct PI_STRUCT PI_IL_6;
// #endif

// *** Reguls_ISR ***
// Handle Uin conversion
__interrupt void adc_Uin_isr(void){
    adc_Uin_Results = AdcaResultRegs.ADCRESULT0;
    dataADC.Voltage_in = adc_Uin_Results*(0.2128) - 397.67;
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;
    EPwm1Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdcaRegs.ADCINTOVF.bit.ADCINT1){
        AdcaRegs.ADCINTOVFCLR.bit.ADCINT1 = 1; //clear INT2 overflow flag
        AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1; //clear INT2 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
    cntr[0]++;
}

// Handle Uout conversion
__interrupt void adc_Uout_isr(void){
    adc_Uout_Results = AdcaResultRegs.ADCRESULT1;
    dataADC.Voltage_out = adc_Uout_Results*(0.2029) - 381.36;
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
    EPwm1Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdcaRegs.ADCINTOVF.bit.ADCINT2){
        AdcaRegs.ADCINTOVFCLR.bit.ADCINT2 = 1; //clear INT2 overflow flag
        AdcaRegs.ADCINTFLGCLR.bit.ADCINT2 = 1; //clear INT2 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_U, dataADC.U_REF, dataADC.Voltage_out);
    cntr[1]++;
}

// Handle Phase 1 ADC conversion
__interrupt void adc_phs_1_isr(void){
    adc_phs1_Results = AdcdResultRegs.ADCRESULT0;
    dataADC.Current_1 = adc_phs1_Results*(0.007928) - 15.30;
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
    EPwm1Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdcdRegs.ADCINTOVF.bit.ADCINT2){
        AdcdRegs.ADCINTOVFCLR.bit.ADCINT2 = 1; //clear INT2 overflow flag
        AdcdRegs.ADCINTFLGCLR.bit.ADCINT2 = 1; //clear INT2 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_IL_1, PI_U.out*CURRENT_SCALE_RATIO, dataADC.Current_1);
    EPwm1Regs.CMPA.bit.CMPA = PI_IL_1.out * 500;
    cntr[2]++;
}

// Handle Phase 2 ADC conversion
__interrupt void adc_phs_2_isr(void){
    adc_phs2_Results = AdcdResultRegs.ADCRESULT1;
    dataADC.Current_2 = adc_phs2_Results*(0.007898) - 15.38;
    AdcdRegs.ADCINTFLGCLR.bit.ADCINT3 = 1;
    EPwm2Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdcdRegs.ADCINTOVF.bit.ADCINT3){
        AdcdRegs.ADCINTOVFCLR.bit.ADCINT3 = 1; //clear INT2 overflow flag
        AdcdRegs.ADCINTFLGCLR.bit.ADCINT3 = 1; //clear INT2 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_IL_2, PI_U.out*CURRENT_SCALE_RATIO, dataADC.Current_2);
    EPwm2Regs.CMPA.bit.CMPA = PI_IL_2.out * 500;
    cntr[3]++;
}

// Handle Phase 3 ADC conversion
__interrupt void adc_phs_3_isr(void){
    adc_phs3_Results = AdcaResultRegs.ADCRESULT2;
    dataADC.Current_3 = adc_phs3_Results*(0.007917) - 15.46;
    AdcaRegs.ADCINTFLGCLR.bit.ADCINT4 = 1;
    EPwm3Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdcaRegs.ADCINTOVF.bit.ADCINT4){
        AdcaRegs.ADCINTOVFCLR.bit.ADCINT4 = 1;
        AdcaRegs.ADCINTFLGCLR.bit.ADCINT4 = 1;
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_IL_3, PI_U.out*CURRENT_SCALE_RATIO, dataADC.Current_3);
    EPwm3Regs.CMPA.bit.CMPA = PI_IL_3.out * 500;
    cntr[4]++;
}

// Handle Phase 4 ADC conversion
__interrupt void adc_phs_4_isr(void){
    adc_phs4_Results = AdccResultRegs.ADCRESULT0;
    dataADC.Current_4 = adc_phs4_Results*(0.007888) - 15.33;
    AdccRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
    EPwm4Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdccRegs.ADCINTOVF.bit.ADCINT2){
        AdccRegs.ADCINTOVFCLR.bit.ADCINT2 = 1; //clear INT1 overflow flag
        AdccRegs.ADCINTFLGCLR.bit.ADCINT2 = 1; //clear INT1 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_IL_4, PI_U.out*CURRENT_SCALE_RATIO, dataADC.Current_4);
    EPwm4Regs.CMPA.bit.CMPA = PI_IL_4.out * 500;
//    // Spike for Sanity check
//    GpioDataRegs.GPBSET.bit.GPIO32 = 1;
//    DELAY_US(0.05);
//    GpioDataRegs.GPBCLEAR.bit.GPIO32 = 1;
    cntr[5]++;
}

// Handle Phase 5 ADC conversion
__interrupt void adc_phs_5_isr(void){
    adc_phs5_Results = AdccResultRegs.ADCRESULT1;
    dataADC.Current_5 = adc_phs5_Results*(0.007896) - 15.46;
    AdccRegs.ADCINTFLGCLR.bit.ADCINT3 = 1;
    EPwm5Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdccRegs.ADCINTOVF.bit.ADCINT3){
        AdccRegs.ADCINTOVFCLR.bit.ADCINT3 = 1; //clear INT2 overflow flag
        AdccRegs.ADCINTFLGCLR.bit.ADCINT3 = 1; //clear INT2 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_IL_5, PI_U.out*CURRENT_SCALE_RATIO, dataADC.Current_5);
    EPwm5Regs.CMPA.bit.CMPA = PI_IL_5.out * 500;
    cntr[6]++;
}

// Handle Phase 6 ADC conversion
__interrupt void adc_phs_6_isr(void){
    adc_phs6_Results = AdccResultRegs.ADCRESULT2;
    dataADC.Current_6 = adc_phs6_Results*(0.007890) - 15.23;
    AdccRegs.ADCINTFLGCLR.bit.ADCINT4 = 1;
    EPwm6Regs.ETCLR.bit.SOCA = 1;
    if(1 == AdccRegs.ADCINTOVF.bit.ADCINT4){
        AdccRegs.ADCINTOVFCLR.bit.ADCINT4 = 1; //clear INT3 overflow flag
        AdccRegs.ADCINTFLGCLR.bit.ADCINT4 = 1; //clear INT3 flag
    }
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP10;
    Pi_Calc(&PI_IL_6, PI_U.out*CURRENT_SCALE_RATIO, dataADC.Current_6);
    EPwm6Regs.CMPA.bit.CMPA = PI_IL_6.out * 500;
    cntr[7]++;
}

// ================== EO_ISR_Defines ========================
// ### EOF ###


