#include "F28x_Project.h"
#include "hdrs/setupCAN.h"
#include "hdrs/dataCAN.h"
#include "hdrs/setupPWM.h"


// ========== Definitions =============

// External flags indicating CAN transmission and reception events
extern volatile bool can_tx_flag, can_rx_flag;

// CAN message structures for transmission
struct CAN_TX_MESSAGE_STRUCT txMessage1;
struct CAN_TX_MESSAGE_STRUCT txMessage2;

// CAN message structures for reception
struct CAN_RX_MESSAGE_STRUCT rxMessage1;
struct CAN_RX_MESSAGE_STRUCT rxMessage2;

// Define a function pointer type for control actions
typedef void (*CtrlActionFunc)(void);

// Arrays of function pointers for turning control bits ON and OFF
CtrlActionFunc bitsOnFunctions[] = {
    ProtectionON, PWM1_ON, PWM2_ON, PWM3_ON, PWM4_ON, PWM5_ON, PWM6_ON, ResetON
};
CtrlActionFunc bitsOffFunctions[] = {
    ProtectionOFF, PWM1_OFF, PWM2_OFF, PWM3_OFF, PWM4_OFF, PWM5_OFF, PWM6_OFF, ResetOFF
};
// ========== End of Definitions =============

// ========== Data Conversion Functions =============

// Convert a float value to two bytes for CAN transmission
void CanConvertFloatToBytes(uint16_t *DestinationStartAddress, volatile float *inputData, float scaling, float offset) {
    volatile float tempVal;
    volatile uint16_t temp_uint;

#ifdef CCS
    DINT;  // Disable interrupts for atomic operation
#endif
    tempVal = *inputData;  // Safely read the input data
#ifdef CCS
    EINT;  // Re-enable interrupts
#endif

    // Apply scaling and offset to convert float to integer
    temp_uint = (uint16_t)(((tempVal * scaling) + offset));

    // Split the integer into two bytes and store in destination
    DestinationStartAddress[0] = (temp_uint & 0xFF00) >> 8; // High byte
    DestinationStartAddress[1] = (temp_uint & 0x00FF);       // Low byte
}

// Convert two bytes from CAN reception to a float value
float CanConvertBytesToFloat(uint16_t *SourceStartAddress, float scaling, float offset) {
    volatile uint16_t tempHighByte;
    volatile uint16_t tempLowByte;
    volatile uint16_t tempCompleteDataInt;
    volatile float tempVal;

#ifdef CCS
    DINT;  // Disable interrupts for atomic operation
#endif
    tempHighByte = SourceStartAddress[0]; // Read high byte
    tempLowByte = SourceStartAddress[1];  // Read low byte
#ifdef CCS 
    EINT;  // Re-enable interrupts
#endif

    // Combine high and low bytes into a single integer
    tempCompleteDataInt = (tempHighByte << 8) | tempLowByte;

    // Apply scaling and offset to convert integer back to float
    tempVal = (tempCompleteDataInt * scaling - offset);

    return tempVal;
}
// ========== End of Data Conversion Functions ============

// ========== CAN Communication Functions =============

// Prepare CAN transmission data based on the message ID
int dataCANTx(struct CAN_TX_MESSAGE_STRUCT *msg, uint32_t CanID) {
    msg->CanID = CanID;                      // Set the CAN message ID
    memset(msg->data, OFFSET, sizeof(msg->data)); // Clear the data array

    switch (CanID) {
        case 0x03: // Sending current measurements 1-4
            CanConvertFloatToBytes((uint16_t*)&msg->data[0], &dataADC.Current_1, CURRENTS_SCALE_FACTOR, OFFSET);
            CanConvertFloatToBytes((uint16_t*)&msg->data[2], &dataADC.Current_2, CURRENTS_SCALE_FACTOR, OFFSET);
            CanConvertFloatToBytes((uint16_t*)&msg->data[4], &dataADC.Current_3, CURRENTS_SCALE_FACTOR, OFFSET);
            CanConvertFloatToBytes((uint16_t*)&msg->data[6], &dataADC.Current_4, CURRENTS_SCALE_FACTOR, OFFSET);
            break;
        case 0x04: // Sending current measurements 5-6 and voltages
            CanConvertFloatToBytes((uint16_t*)&msg->data[0], &dataADC.Current_5, CURRENTS_SCALE_FACTOR, OFFSET);
            CanConvertFloatToBytes((uint16_t*)&msg->data[2], &dataADC.Current_6, CURRENTS_SCALE_FACTOR, OFFSET);
            CanConvertFloatToBytes((uint16_t*)&msg->data[4], &dataADC.Voltage_in, VOLTAGE_SCALE_FACTOR, OFFSET);
            CanConvertFloatToBytes((uint16_t*)&msg->data[6], &dataADC.Voltage_out, VOLTAGE_SCALE_FACTOR, OFFSET);
            break;
        default:
            return -1; // Unknown CAN ID
    }
    return 0; // Success
}

// Process received CAN data based on the message ID
float dataCANRx(struct CAN_RX_MESSAGE_STRUCT *msg, uint32_t CanID) {
    CAN_MessageRead(msg);     // Read the CAN message
    msg->CanID = CanID;       // Set the CAN message ID
    uint16_t byte0 = msg->data[0]; // First byte of the data

    if (!msg->newDataAvailable) {
        return -1; // No new data available
    }

    switch (CanID) {
        case 0x07:
            // Control bits for PWM channels
            if (byte0 & 0x01) bitsOnFunctions[0](); else bitsOffFunctions[0](); // Protection
            if (byte0 & 0x02) bitsOnFunctions[1](); else bitsOffFunctions[1](); // PWM1
            if (byte0 & 0x04) bitsOnFunctions[2](); else bitsOffFunctions[2](); // PWM2
            if (byte0 & 0x08) bitsOnFunctions[3](); else bitsOffFunctions[3](); // PWM3
            if (byte0 & 0x10) bitsOnFunctions[4](); else bitsOffFunctions[4](); // PWM4
            if (byte0 & 0x20) bitsOnFunctions[5](); else bitsOffFunctions[5](); // PWM5
            if (byte0 & 0x40) bitsOnFunctions[6](); else bitsOffFunctions[6](); // PWM6
            if (byte0 & 0x80) bitsOnFunctions[7](); else bitsOffFunctions[7](); // Reset
            
            dataADC.U_REF = CanConvertBytesToFloat((uint16_t*)&msg->data[2], VOLTAGE_SCALE_FACTOR, OFFSET);
            break;
        case 0x0B:
            dataADC.U_REF = CanConvertBytesToFloat((uint16_t*)&msg->data[0], VOLTAGE_SCALE_FACTOR, OFFSET);
            break;
        default:
            break; // Unknown CAN ID
    }

    msg->newDataAvailable = 0; // Reset the flag
    return 0; // Success
}

// Initialize CAN mailboxes for transmission and reception
void mailboxingCAN(){
    // Transmission mailboxes
    CAN_MailBoxTxSetAndInit(&txMessage1, 13, CAN_11BIT, 0x03, 8);
    CAN_MailBoxTxSetAndInit(&txMessage2, 14, CAN_11BIT, 0x04, 8);

    // Reception mailboxes
    CAN_MailBoxRxSetAndInit(&rxMessage1, 2, CAN_11BIT, 0x07, 8, 0x7FF);
    CAN_MailBoxRxSetAndInit(&rxMessage2, 3, CAN_11BIT, 0x0B, 2, 0x7FF);
}

// Handle CAN transmission events
void handleCANTx(void) {
    if (can_tx_flag) {
        // Prepare and send message 1
        dataCANTx(&txMessage1, txMessage1.CanID);
        CAN_MessageSend(&txMessage1);

        // Prepare and send message 2
        dataCANTx(&txMessage2, txMessage2.CanID);
        CAN_MessageSend(&txMessage2);

        can_tx_flag = false; // Reset the flag
    }
}

// Handle CAN reception events
void handleCANRx(void) {
    if (can_rx_flag) {
        // Process received messages
        dataCANRx(&rxMessage1, rxMessage1.CanID);
        rxMessage1.newDataAvailable = 0;

        dataCANRx(&rxMessage2, rxMessage2.CanID);
        rxMessage2.newDataAvailable = 0;

        can_rx_flag = false; // Reset the flag
    }
}
// ========== End of CAN Communication Functions ============

// ========== Control Action Functions =============

// Turn Protection ON/OFF
void ProtectionON(void)  { GpioDataRegs.GPASET.bit.GPIO19 = 1; }
void ProtectionOFF(void) { GpioDataRegs.GPACLEAR.bit.GPIO19 = 1; }

// Turn Reset ON/OFF
void ResetON(void)  { GpioDataRegs.GPASET.bit.GPIO21 = 1; }
void ResetOFF(void) { GpioDataRegs.GPACLEAR.bit.GPIO21 = 1; }

// Control PWM channels 1-6
void PWM1_ON(void) { EALLOW; EPwm1Regs.TZCLR.all = 0x0000; EDIS; }
void PWM1_OFF(void){ EALLOW; EPwm1Regs.TZCLR.all = 0xC000; EPwm1Regs.TZFRC.all = 0x0002; EDIS; }

void PWM2_ON(void) { EALLOW; EPwm2Regs.TZCLR.all = 0x0000; EDIS; }
void PWM2_OFF(void){ EALLOW; EPwm2Regs.TZCLR.all = 0xC000; EPwm2Regs.TZFRC.all = 0x0002; EDIS; }

void PWM3_ON(void) { EALLOW; EPwm3Regs.TZCLR.all = 0x0000; EDIS; }
void PWM3_OFF(void){ EALLOW; EPwm3Regs.TZCLR.all = 0xC000; EPwm3Regs.TZFRC.all = 0x0002; EDIS; }

void PWM4_ON(void) { EALLOW; EPwm4Regs.TZCLR.all = 0x0000; EDIS; }
void PWM4_OFF(void){ EALLOW; EPwm4Regs.TZCLR.all = 0xC000; EPwm4Regs.TZFRC.all = 0x0002; EDIS; }

void PWM5_ON(void) { EALLOW; EPwm5Regs.TZCLR.all = 0x0000; EDIS; }
void PWM5_OFF(void){ EALLOW; EPwm5Regs.TZCLR.all = 0xC000; EPwm5Regs.TZFRC.all = 0x0002; EDIS; }

void PWM6_ON(void) { EALLOW; EPwm6Regs.TZCLR.all = 0x0000; EDIS; }
void PWM6_OFF(void){ EALLOW; EPwm6Regs.TZCLR.all = 0xC000; EPwm6Regs.TZFRC.all = 0x0002; EDIS; }
// ========== End of Control Action Functions ============
