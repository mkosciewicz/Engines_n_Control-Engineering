#include "F28x_Project.h"
#include "hdrs/setupISR.h"

void setupISR() {
    EALLOW;
    
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
    
    PieCtrlRegs.PIEIER1.bit.INTx7=1;          // Timer0 TINT0

    PieCtrlRegs.PIEIER1.bit.INTx1 = 1;        //ADCINTA1
    PieCtrlRegs.PIEIER10.bit.INTx2 = 1;       //ADCINTA2

    PieCtrlRegs.PIEIER10.bit.INTx14 = 1;        //ADCINTD2
    PieCtrlRegs.PIEIER10.bit.INTx15 = 1;      //ADCINTD3
    PieCtrlRegs.PIEIER10.bit.INTx4 = 1;      //ADCINTA4

    PieCtrlRegs.PIEIER10.bit.INTx10 = 1;        //ADCINTC2
    PieCtrlRegs.PIEIER10.bit.INTx11 = 1;      //ADCINTC3
    PieCtrlRegs.PIEIER10.bit.INTx12 = 1;      //ADCINTC4

    PieCtrlRegs.PIEIFR1.all = 0;
    IER |= M_INT1 | M_INT10 | M_INT13;
    EINT;
    ERTM;
    EDIS;
}

