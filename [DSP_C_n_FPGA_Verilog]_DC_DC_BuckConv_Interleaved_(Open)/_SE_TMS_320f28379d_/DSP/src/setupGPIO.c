#include "F28x_Project.h"
#include "hdrs/setupGPIO.h"

void setupGPIO() {
    EALLOW;

    // *** EPWM Lines ***
    GpioCtrlRegs.GPAGMUX1.bit.GPIO0 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1; // EPWM1A
    GpioCtrlRegs.GPAPUD.bit.GPIO0 = 1;  // Disable pull-up
    GpioDataRegs.GPACLEAR.bit.GPIO0 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO0 = 1;  // Set as output

    GpioCtrlRegs.GPAGMUX1.bit.GPIO2 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 1; // EPWM2A
    GpioCtrlRegs.GPAPUD.bit.GPIO2 = 1;  // Disable pull-up
    GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO2 = 1;  // Set as output

    GpioCtrlRegs.GPAGMUX1.bit.GPIO4 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 1; // EPWM3A
    GpioCtrlRegs.GPAPUD.bit.GPIO4 = 1;  // Disable pull-up
    GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO4 = 1;  // Set as output

    GpioCtrlRegs.GPAGMUX1.bit.GPIO6 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 1; // EPWM4A
    GpioCtrlRegs.GPAPUD.bit.GPIO6 = 1;  // Disable pull-up
    GpioDataRegs.GPACLEAR.bit.GPIO6 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO6 = 1;  // Set as output

    GpioCtrlRegs.GPAGMUX1.bit.GPIO8 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 1; // EPWM5A
    GpioCtrlRegs.GPAPUD.bit.GPIO8 = 1;  // Disable pull-up
    GpioDataRegs.GPACLEAR.bit.GPIO8 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO8 = 1;  // Set as output

    GpioCtrlRegs.GPAGMUX1.bit.GPIO10 = 0;
    GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 1; // EPWM6A
    GpioCtrlRegs.GPAPUD.bit.GPIO10 = 1;  // Disable pull-up
    GpioDataRegs.GPACLEAR.bit.GPIO10 = 1;
    GpioCtrlRegs.GPADIR.bit.GPIO10 = 1;  // Set as output

    // *** SO_CAN Lines ***
    // CANRX
    GpioCtrlRegs.GPBGMUX1.bit.GPIO39 = 0x01;
    GpioCtrlRegs.GPBMUX1.bit.GPIO39 = 0x02;
    GpioCtrlRegs.GPBPUD.bit.GPIO39 = 1;
    GpioDataRegs.GPBCLEAR.bit.GPIO39 = 1;
    GpioCtrlRegs.GPBDIR.bit.GPIO39 = 0; // Set as input

    // CANTX
    GpioCtrlRegs.GPBGMUX1.bit.GPIO38 = 0x01;
    GpioCtrlRegs.GPBMUX1.bit.GPIO38 = 0x02;
    GpioCtrlRegs.GPBPUD.bit.GPIO38 = 1;
    GpioDataRegs.GPBCLEAR.bit.GPIO38 = 1;
    GpioCtrlRegs.GPBDIR.bit.GPIO38 = 1; // Set as output

    // *** SO_Ctrl_Bits Lines ***
    // ResetBit
    GpioCtrlRegs.GPAMUX2.bit.GPIO21 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO21 = 1; // Set as output

    // TestBit
    GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO19 = 1; // Set as output

    // *** SO_Else_Stuff Lines ***
    // Sanity check bit > Create a Spike on GPIO32
    // GpioCtrlRegs.GPBDIR.bit.GPIO32 = 1;
    // GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0;
    // GpioCtrlRegs.GPBGMUX1.bit.GPIO32 = 0;

    EDIS;
}
