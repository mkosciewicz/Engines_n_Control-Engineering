#include "F28x_Project.h"
#include "hdrs/dataCAN.h"
#include "hdrs/initReglsPI.h"
#include "hdrs/reglsPI.h"

extern struct PI_STRUCT PI_U;
extern struct PI_STRUCT PI_IL_1;
extern struct PI_STRUCT PI_IL_2;
extern struct PI_STRUCT PI_IL_3;
extern struct PI_STRUCT PI_IL_4;
extern struct PI_STRUCT PI_IL_5;
extern struct PI_STRUCT PI_IL_6;

void initReglsPI() {
    Pi_Init(&PI_U, TS, voltageKP, voltageKI, REG_LOWER_LIM, VREG_UPPER_LIM_AMP);
    Pi_Init(&PI_IL_1, TS, currentKP, currentKI, REG_LOWER_LIM, IREG_UPPER_LIM);
    Pi_Init(&PI_IL_2, TS, currentKP, currentKI, REG_LOWER_LIM, IREG_UPPER_LIM);
    Pi_Init(&PI_IL_3, TS, currentKP, currentKI, REG_LOWER_LIM, IREG_UPPER_LIM);
    Pi_Init(&PI_IL_4, TS, currentKP, currentKI, REG_LOWER_LIM, IREG_UPPER_LIM);
    Pi_Init(&PI_IL_5, TS, currentKP, currentKI, REG_LOWER_LIM, IREG_UPPER_LIM);
    Pi_Init(&PI_IL_6, TS, currentKP, currentKI, REG_LOWER_LIM, IREG_UPPER_LIM);
}

