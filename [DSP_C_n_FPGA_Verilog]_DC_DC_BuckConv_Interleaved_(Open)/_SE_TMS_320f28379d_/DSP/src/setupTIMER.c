#include "F28x_Project.h"
#include "hdrs/setupTIMER.h"

void setupTxTimer(void){

    EALLOW;
    CpuTimer0Regs.TCR.bit.TSS=1;
    CpuTimer0Regs.TPR.bit.TDDR = 0xC8;
    CpuTimer0Regs.TPRH.bit.TDDRH = 0x00;

    // To define the PRD value for 10e5
    CpuTimer0Regs.PRD.bit.MSW = 0x0001;
    CpuTimer0Regs.PRD.bit.LSW = 0x86A0;
    // Eventual: CpuTimer0Regs.PRD.all=1000000;

    CpuTimer0Regs.TCR.bit.TIE=1;
    CpuTimer0Regs.TCR.bit.TRB=1;
    CpuTimer0Regs.TCR.bit.TSS=0;

    EDIS;
}

void setupRxTimer(void) {
    EALLOW;
    CpuTimer1Regs.TCR.bit.TSS = 1;  // Stop the timer
    CpuTimer1Regs.TPR.bit.TDDR = 0xC8;  // Timer divider
    CpuTimer1Regs.TPRH.bit.TDDRH = 0x00;

    CpuTimer1Regs.PRD.bit.MSW = 0x0001;
    CpuTimer1Regs.PRD.bit.LSW = 0x86A0;
    
    CpuTimer1Regs.TCR.bit.TIE = 1;  // Enable timer interrupt
    CpuTimer1Regs.TCR.bit.TRB = 1;  // Reload the timer
    CpuTimer1Regs.TCR.bit.TSS = 0;  // Start the timer

    EDIS;
}

void setupTIMER(){

	setupTxTimer();
	setupRxTimer();
}

