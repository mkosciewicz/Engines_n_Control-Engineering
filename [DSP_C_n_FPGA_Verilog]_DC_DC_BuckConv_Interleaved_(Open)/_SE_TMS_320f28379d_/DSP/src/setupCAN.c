#include "F28x_Project.h"
#include "hdrs/setupCAN.h"

// ========== Global Variables =============
extern struct CAN_RX_MESSAGE_STRUCT rxMessage1;
extern struct CAN_RX_MESSAGE_STRUCT rxMessage2;

unsigned char ucTXMsgData[MESSAGE_DATA_SIZE] = {0x1, 0x2, 0x3, 0x4};
unsigned char ucRXMsgData[MESSAGE_DATA_SIZE] = {0, 0, 0, 0};
uint32_t messageSize = MESSAGE_DATA_SIZE;
volatile unsigned long msgCount = 0;
volatile unsigned long errFlag = 0;

// ========== CAN Bit Timing Values =============
const uint16_t canBitValues[] = {
    0x1100, // TSEG2 2, TSEG1 2, SJW 1, Divide 5
    0x1200, // TSEG2 2, TSEG1 3, SJW 1, Divide 6
    0x2240, // TSEG2 3, TSEG1 3, SJW 2, Divide 7
    0x2340, // TSEG2 3, TSEG1 4, SJW 2, Divide 8
    0x3340, // TSEG2 4, TSEG1 4, SJW 2, Divide 9
    0x3440, // TSEG2 4, TSEG1 5, SJW 2, Divide 10
    0x3540, // TSEG2 4, TSEG1 6, SJW 2, Divide 11
    0x3640, // TSEG2 4, TSEG1 7, SJW 2, Divide 12
    0x3740  // TSEG2 4, TSEG1 8, SJW 2, Divide 13
};

// ========== Initialization Functions =============
void setupCAN(void) {
    EALLOW;  
    CAN_PeripheralModuleInit(500000); 
    EDIS;
}

void CAN_clearMailBoxes(void) {
    while (CanbRegs.CAN_IF1CMD.bit.Busy) {
        __asm(" NOP");  //wait for busy bit to clear - from example
    }
    //clear all message boxes
    int16_t iMsg;

    CanbRegs.CAN_CTL.bit.Init = 1; // init mode
    CanbRegs.CAN_CTL.bit.CCE = 1;  //enable register modification
    CanbRegs.CAN_CTL.bit.SWR = 1; //set CAN in reset state

    CanbRegs.CAN_IF1CMD.bit.DIR = 1;
    CanbRegs.CAN_IF1CMD.bit.Arb = 1;
    CanbRegs.CAN_IF1CMD.bit.Control = 1;
    CanbRegs.CAN_IF1ARB.all = 0;
    CanbRegs.CAN_IF1MCTL.all = 0;
    CanbRegs.CAN_IF1CMD.bit.ClrIntPnd = 1; // clear any pending interrupt


    CanbRegs.CAN_IF2CMD.bit.DIR = 1;
    CanbRegs.CAN_IF2CMD.bit.Arb = 1;
    CanbRegs.CAN_IF2CMD.bit.Control = 1;
    CanbRegs.CAN_IF2ARB.all = 0;
    CanbRegs.CAN_IF2MCTL.all = 0;
    CanbRegs.CAN_IF2CMD.bit.ClrIntPnd = 1; // clear any pending interrupt

    for (iMsg = 1; iMsg <= 32; iMsg += 2) {
        // Wait for busy bit to clear
        while (CanbRegs.CAN_IF1CMD.bit.Busy) { __asm(" NOP"); }
        // Initiate programming the message object
        CanbRegs.CAN_IF1CMD.bit.MSG_NUM = iMsg;
        // Wait for busy bit to clear
        while (CanbRegs.CAN_IF2CMD.bit.Busy) { __asm(" NOP"); }
        // Initiate programming the message object
        CanbRegs.CAN_IF2CMD.bit.MSG_NUM = iMsg + 1;
    }
}

void CAN_PeripheralModuleInit(unsigned long bitrate) { //bitrate in bits per second
	EALLOW;
	//clocking
	CpuSysRegs.PCLKCR10.bit.CAN_B = 0; //disable CAN_B_GATING
	DevCfgRegs.CPUSEL8.bit.CAN_B = 0; //select CPU1 for CANB
	ClkCfgRegs.CLKSRCCTL2.bit.CANBBCLKSEL = 0x0; //SYSCLK as clock source
	CpuSysRegs.PCLKCR10.bit.CAN_B = 1; //enable CAN_B_GATING

	// input qualification
	GpioCtrlRegs.GPBQSEL1.bit.GPIO39 = 0x03; // async sampling (page 2281)
	GpioCtrlRegs.GPBQSEL1.bit.GPIO38 = 0x03; // async sampling (page 2281)

	CAN_clearMailBoxes(); // also clears any message interrupts
	volatile uint32_t discardRead = CanbRegs.CAN_ES.all; // Acknowledge any pending status interrupts.
	CanbRegs.CAN_GLB_INT_CLR.bit.INT0_FLG_CLR = 1;         // clear int 0 flag
	CanbRegs.CAN_GLB_INT_CLR.bit.INT1_FLG_CLR = 1;         // clear int 1 flag
	CAN_BitrateSet(200e6, bitrate);

	// interrupt stuff
	CanbRegs.CAN_CTL.bit.IE0 = 1;
	CanbRegs.CAN_GLB_INT_EN.bit.GLBINT0_EN = 1; // enable line 0 interrupt -> PIE
	PieVectTable.CANB0_INT = &CANA_interrupt0;
	PieCtrlRegs.PIEIER9.bit.INTx7 = 1;
	IER|=M_INT9;

	//auto bus on feature
	CanbRegs.CAN_CTL.bit.ABO = 1;
	EDIS;

}

__interrupt void CANA_interrupt0(void) { // GROUP 9, channel 5
    //first determine the cause of an interrupt:
    volatile uint16_t CAN_interruptPendingNumber;
    CAN_interruptPendingNumber = CanbRegs.CAN_INT.bit.INT0ID; // Number of the mailbox with pending interrupt

    //read pending message:
    //warning: this have to be modified when Rx Messages are added/changed
    switch (CAN_interruptPendingNumber) {

    case 2:
        CAN_MessageRead(&rxMessage1);
        rxMessage1.MessageCounter++;
        break;
    case 3:
        CAN_MessageRead(&rxMessage2);
        rxMessage2.MessageCounter++;
        break;
    default:    //in case of any non defined receive
        break;
    }

    //clear message flag
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;
    CAN_IF1CMD_SHADOW.all = 0; // clear whole shadow register
    CAN_IF1CMD_SHADOW.bit.ClrIntPnd = 1; // clear interrupt bit;
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = CAN_interruptPendingNumber;

    while (CanbRegs.CAN_IF1CMD.bit.Busy) {
        __asm(" NOP"); //wait for interface to be ready
    }
    CanbRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all; //write register

    //Clear CAN global flag
    CanbRegs.CAN_GLB_INT_CLR.bit.INT0_FLG_CLR = 1; // clear flag of interrupt int0
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;
}

uint32_t CAN_BitrateSet(uint32_t sourceClockFreqHz, uint32_t bitRate) { //from examples
    uint32_t desiredRatio;
    uint32_t canBits;
    uint32_t preDivide;
    uint32_t regValue;
    uint16_t canControlValue;

    // Calculate the desired clock rate.
    desiredRatio = sourceClockFreqHz / bitRate;
    // Make sure that the Desired Ratio is not too large.  This enforces the
    // requirement that the bit rate is larger than requested.
    if ((sourceClockFreqHz / desiredRatio) > bitRate) {
        desiredRatio += 1;
    }
    // Check all possible values to find a matching dacValue.
    while (desiredRatio <= CAN_MAX_PRE_DIVISOR * CAN_MAX_BIT_DIVISOR) {
        // Loop through all possible CAN bit divisors.
        for (canBits = CAN_MAX_BIT_DIVISOR; canBits >= CAN_MIN_BIT_DIVISOR; canBits--) {
            // For a given CAN bit divisor save the pre divisor.
            preDivide = desiredRatio / canBits;
            // If the calculated divisors match the desired clock ratio then
            // return these bit rate and set the CAN bit timing.
            if ((preDivide * canBits) == desiredRatio) {
                // Start building the bit timing dacValue by adding the bit timing
                // in time quanta.
                regValue = canBitValues[canBits - CAN_MIN_BIT_DIVISOR];
                // To set the bit timing register, the controller must be placed
                // in init mode (if not already), and also configuration change bit enabled.
                /// The state of the register should be saved so it can be restored.
                canControlValue = CanbRegs.CAN_CTL.all;
                CanbRegs.CAN_CTL.bit.Init = 1;
                CanbRegs.CAN_CTL.bit.CCE = 1;
                // Now add in the pre-scalar on the bit rate.
                regValue |= ((preDivide - 1) & CAN_BTR_BRP_M) | (((preDivide - 1) << 10) & CAN_BTR_BRPE_M);
                // Set the clock bits in the and the bits of the pre-scalar.
                CanbRegs.CAN_BTR.all = regValue;
                // Restore the saved CAN Control register.
                CanbRegs.CAN_CTL.all = canControlValue;
                // Return the computed bit rate.
                return(sourceClockFreqHz / (preDivide * canBits));
            }
        }
        // Move the divisor up one and look again.  Only in rare cases are
        // more than 2 loops required to find the dacValue.
        desiredRatio++;
    }
    return 0;
}

void CAN_MessageSend(struct CAN_TX_MESSAGE_STRUCT *CanTxMessage) {
    while (CanbRegs.CAN_IF1CMD.bit.Busy) {
        __asm(" NOP");
    }
    uint16_t index;
    for (index = 0; index < CanTxMessage->length; index++) {
        switch (index) {
        case 0:
            CanbRegs.CAN_IF1DATA.bit.Data_0 = CanTxMessage->data[index];
            break;
        case 1:
            CanbRegs.CAN_IF1DATA.bit.Data_1 = CanTxMessage->data[index];
            break;
        case 2:
            CanbRegs.CAN_IF1DATA.bit.Data_2 = CanTxMessage->data[index];
            break;
        case 3:
            CanbRegs.CAN_IF1DATA.bit.Data_3 = CanTxMessage->data[index];
            break;
        case 4:
            CanbRegs.CAN_IF1DATB.bit.Data_4 = CanTxMessage->data[index];
            break;
        case 5:
            CanbRegs.CAN_IF1DATB.bit.Data_5 = CanTxMessage->data[index];
            break;
        case 6:
            CanbRegs.CAN_IF1DATB.bit.Data_6 = CanTxMessage->data[index];
            break;
        case 7:
            CanbRegs.CAN_IF1DATB.bit.Data_7 = CanTxMessage->data[index];
            break;
        }
    }

    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;
    CAN_IF1CMD_SHADOW.all = 0; // clear whole shadow register
    CAN_IF1CMD_SHADOW.bit.DIR = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_A = 1;

    if (CanTxMessage->length > 4) {
        CAN_IF1CMD_SHADOW.bit.DATA_B = 1;
    }
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = CanTxMessage->IntMsgNum;

    CAN_IF1CMD_SHADOW.bit.TXRQST = 1;
    CanbRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all; // send data!
    CanTxMessage->MessageCounter++;
}

void CAN_MailBoxTxSetAndInit(struct CAN_TX_MESSAGE_STRUCT *CanTxMessage, uint16_t IntMsgNum, enum CAN_IDTYPE IdType, uint32_t Id, uint16_t length) {

    CanTxMessage->CAN_IdType = IdType;
    CanTxMessage->CanID = Id;
    CanTxMessage->IntMsgNum = IntMsgNum;
    CanTxMessage->length = length;
    CanTxMessage->MessageCounter = 0;

    unsigned int i;
    for (i = 0; i < 8; i++) {
        CanTxMessage->data[i] = 0;
    }

    volatile union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW; // create shadow reg
    CAN_IF1CMD_SHADOW.all = 0; //clear shadow reg

    while (CanbRegs.CAN_IF1CMD.bit.Busy) {
        __asm(" NOP");
    }
    CanbRegs.CAN_IF1MSK.all = 0;
    CanbRegs.CAN_IF1ARB.all = 0;
    CanbRegs.CAN_IF1MCTL.all = 0;
    
    //stuff to configure in message
    CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;
    CAN_IF1CMD_SHADOW.bit.Mask = 1;
    //configuration will be written
    CAN_IF1CMD_SHADOW.bit.DIR = 1;

    CanbRegs.CAN_IF1ARB.bit.Dir = 1; // Tx message type
    if (CanTxMessage->CAN_IdType == CAN_11BIT) {
        CanbRegs.CAN_IF1ARB.bit.ID = (CanTxMessage->CanID << CAN_11BIT_ID_BIT_SHIFT); //set id (11 bit)
        CanbRegs.CAN_IF1ARB.bit.Xtd = 0; // 11 bit ID
    }
    else {
        CanbRegs.CAN_IF1ARB.bit.ID = (CanTxMessage->CanID << CAN_29BIT_ID_BIT_SHIFT); //set id (29 bit)
        CanbRegs.CAN_IF1ARB.bit.Xtd = 1; // 29 bit ID
    }

    CanbRegs.CAN_IF1ARB.bit.MsgVal = 1; //this message is valid

    CanbRegs.CAN_IF1MCTL.bit.DLC = CanTxMessage->length;
    CanbRegs.CAN_IF1MCTL.bit.EoB = 1; //one message transfer

    CAN_IF1CMD_SHADOW.bit.MSG_NUM = CanTxMessage->IntMsgNum;
    CanbRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all; //send to message ram
}

void CAN_MailBoxRxSetAndInit(struct CAN_RX_MESSAGE_STRUCT *CanRxMessage, uint16_t IntMsgNum, enum CAN_IDTYPE IdType, uint32_t Id, uint16_t length, uint32_t Mask) {
	CanRxMessage->CAN_IdType = IdType;
	CanRxMessage->CanID = Id;
	CanRxMessage->IntMsgNum = IntMsgNum;
	CanRxMessage->length = length;
	CanRxMessage->MessageCounter = 0;
	CanRxMessage->CanIDMask = Mask;

	unsigned int i;
	for (i = 0; i < 8; i++) {
		CanRxMessage->data[i] = 0;
	}

	volatile union CAN_IF2CMD_REG CAN_IF2CMD_SHADOW; // create shadow reg
	CAN_IF2CMD_SHADOW.all = 0; //clear shadow reg
	while (CanbRegs.CAN_IF2CMD.bit.Busy) {
		__asm(" NOP");
	}
	CanbRegs.CAN_IF2MSK.all = 0;
	CanbRegs.CAN_IF2ARB.all = 0;
	CanbRegs.CAN_IF2MCTL.all = 0;

	CAN_IF2CMD_SHADOW.bit.Control = 1;
	CAN_IF2CMD_SHADOW.bit.Arb = 1;
	CAN_IF2CMD_SHADOW.bit.Mask = 1;
	CAN_IF2CMD_SHADOW.bit.DIR = 1; // write config data

	CanbRegs.CAN_IF2ARB.bit.Dir = 0; // Rx message type
	CanbRegs.CAN_IF2MCTL.bit.DLC = CanRxMessage->length;
	CanbRegs.CAN_IF2MCTL.bit.EoB = 1; //one message transfer

	//interrupt stuff
	CanbRegs.CAN_IF2MCTL.bit.RxIE = 1; //enable message interrupt
	if (CanRxMessage->IntMsgNum == 32) {
		CanbRegs.CAN_IP_MUX21 |= 0x0; // 32 is controlled by bit 0 (CAN_IP_MUX21)
	}
	else {
		CanbRegs.CAN_IP_MUX21 |= 0x0 << CanRxMessage->IntMsgNum; // set interrupt lint to CANINT0
	}
	//------------------------------
	CAN_IF2CMD_SHADOW.bit.MSG_NUM = CanRxMessage->IntMsgNum;
	if (CanRxMessage->CAN_IdType == CAN_11BIT) {
		CanbRegs.CAN_IF2MSK.bit.Msk = CanRxMessage->CanIDMask << CAN_11BIT_ID_BIT_SHIFT;
		CanbRegs.CAN_IF2MSK.bit.MXtd = 0; // 11 bit mask
		CanbRegs.CAN_IF2ARB.bit.ID = (CanRxMessage->CanID << CAN_11BIT_ID_BIT_SHIFT); //set id (11 bit)
		CanbRegs.CAN_IF2ARB.bit.Xtd = 0;  // 11 bit ID
	}
	else {
		CanbRegs.CAN_IF2MSK.bit.Msk = CanRxMessage->CanIDMask << CAN_29BIT_ID_BIT_SHIFT;
		CanbRegs.CAN_IF2MSK.bit.MXtd = 1; // 29 bit mask
		CanbRegs.CAN_IF2ARB.bit.ID = (CanRxMessage->CanID << CAN_29BIT_ID_BIT_SHIFT); //set id (29 bit)
		CanbRegs.CAN_IF2ARB.bit.Xtd = 1;  // 29 bit ID
	}

	CanbRegs.CAN_IF2MSK.bit.MDir = 1;   // use direction bit
	CanbRegs.CAN_IF2MCTL.bit.UMask = 1; // use mask bits
	CanbRegs.CAN_IF2ARB.bit.MsgVal = 1; //this message is valid
	CanbRegs.CAN_IF2CMD.all = CAN_IF2CMD_SHADOW.all; //send to message ram

	unsigned char temp_i;
	for (temp_i = 0; temp_i < 8; temp_i++) {
		CanRxMessage->data[temp_i] = 0; //clear all data on init
	}
	CanRxMessage->MessageCounter = 0;
	CanRxMessage->newDataAvailable = 0;
}

void CAN_MessageRead(struct CAN_RX_MESSAGE_STRUCT *CanRxMessage) {
	union CAN_IF2CMD_REG CAN_IF2CMD_SHADOW;
	CAN_IF2CMD_SHADOW.all = 0;
	CAN_IF2CMD_SHADOW.bit.Control = 1;
	CAN_IF2CMD_SHADOW.bit.DATA_A = 1;
	if (CanRxMessage->length > 4) {
		CAN_IF2CMD_SHADOW.bit.DATA_B = 1;
	}
	CAN_IF2CMD_SHADOW.bit.MSG_NUM = CanRxMessage->IntMsgNum;
	CAN_IF2CMD_SHADOW.bit.TxRqst = 1; //clear new data flag (can be combined with reading according to reference mnual page 2344)
	CanbRegs.CAN_IF2CMD.all = CAN_IF2CMD_SHADOW.all; //read data

	while (CanbRegs.CAN_IF2CMD.bit.Busy) {
		__asm(" NOP"); //wait to finish reading
	}
	uint16_t index;
	for (index = 0; index < CanRxMessage->length; index++) { //write data to struct
		switch (index) {
		case 0:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATA.bit.Data_0;
			break;
		case 1:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATA.bit.Data_1;
			break;
		case 2:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATA.bit.Data_2;
			break;
		case 3:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATA.bit.Data_3;
			break;
		case 4:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATB.bit.Data_4;
			break;
		case 5:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATB.bit.Data_5;
			break;
		case 6:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATB.bit.Data_6;
			break;
		case 7:
			CanRxMessage->data[index] = CanbRegs.CAN_IF2DATB.bit.Data_7;
			break;
		}
	}
	CanRxMessage->newDataAvailable = 1;
}

// ========== EO_Functions =============
// ### EOF ###

