﻿#include "hdrs/reglsPI.h"

void Pi_Init(struct PI_STRUCT* Pi, float Ts, float kp, float ki, float min, float max) {
    Pi->out = 0;
    Pi->error = 0;
    Pi->integrator = 0;
    Pi->proportional = 0;
    Pi->kp = kp;
    Pi->Ts = Ts;
    Pi->ki = ki;
    Pi->min = min;
    Pi->max = max;
}

void Pi_Calc(struct PI_STRUCT* Pi, float x, float x_zmierzone) {
    float integrator_last = Pi->integrator;
    Pi->error = x - x_zmierzone;
    Pi->proportional = Pi->error * Pi->kp;
    Pi->integrator += Pi->proportional * Pi->ki * Pi->Ts;
    Pi->out = Pi->proportional + Pi->integrator;
    if (Pi->out > Pi->max) {
        Pi->out = Pi->max;
       if ((Pi->integrator < Pi->max) || (Pi->integrator > integrator_last))
       {
           Pi->integrator = integrator_last;
       }
    }
    if (Pi->out < Pi->min) {
        Pi->out = Pi->min;
       if ((Pi->integrator > Pi->min) || (Pi->integrator < integrator_last))
       {
           Pi->integrator = integrator_last;
       }
    }
}

void Pi_Reset(struct PI_STRUCT* Pi) {
    Pi->error = 0;
    Pi->out = 0;
    Pi->integrator = 0;
    Pi->proportional = 0;
}
