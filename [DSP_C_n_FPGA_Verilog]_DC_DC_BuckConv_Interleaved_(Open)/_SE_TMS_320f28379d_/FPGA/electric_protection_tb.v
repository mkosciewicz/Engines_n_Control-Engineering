`timescale 1ns / 1ps

module electric_protection_tb;

reg clk;
reg [6:1] PWM_IN;
reg [6:1] Currents_Comps;
reg Vin_Comp, Vout_Comp;
reg rst_bit, test_bit;
wire [6:1] PWM_OUT, Enable_OUT;
wire RELAY_DC_PLUS, RELAY_DC_MINUS, RELAY_DC_FAN, RELAY_DC_PRECHARGE;

// Instantiate the Unit Under Test (UUT)
electric_protection uut (
    .clk(clk), 
    .PWM_IN(PWM_IN), 
    .Currents_Comps(Currents_Comps), 
    .Vin_Comp(Vin_Comp), 
    .Vout_Comp(Vout_Comp), 
    .rst_bit(rst_bit), 
    .test_bit(test_bit), 
    .PWM_OUT(PWM_OUT), 
    .Enable_OUT(Enable_OUT), 
    .RELAY_DC_PLUS(RELAY_DC_PLUS), 
    .RELAY_DC_MINUS(RELAY_DC_MINUS), 
    .RELAY_DC_FAN(RELAY_DC_FAN), 
    .RELAY_DC_PRECHARGE(RELAY_DC_PRECHARGE)
);

// Generate Clock
always #1 clk = (clk === 1'b0); // 50 MHz Clock

// Testbench sequences
initial begin
    // Initialize Inputs
    clk = 0;
    PWM_IN = 6'b111111;
    Currents_Comps = 6'b111111;
    Vin_Comp = 1'b1;
    Vout_Comp = 1'b1;
    rst_bit = 1'b1; // Active high reset
    test_bit = 1'b0;

    // Wait for global reset
    #20;
    
    // Remove reset
    rst_bit = 1'b0;
    #20;

    // Test Condition 1: Overcurrent
    Currents_Comps = 6'b000000; // Simulate overcurrent condition
    #120;
    Currents_Comps = 6'b111111; // Reset condition

    #120;
    rst_bit = 1'b1; // Active high reset
    #100;
    rst_bit = 1'b0;
    #120;
    
    // Test Condition 2: Overvoltage
    Vout_Comp = 1'b0; // Simulate overvoltage condition
    #120;
    Vout_Comp = 1'b1; // Reset condition

    #120;
    rst_bit = 1'b1; // Active high reset
    #100;
    rst_bit = 1'b0;
    #120;

    // Test Condition 3: Undervoltage
    Vin_Comp = 1'b0; // Simulate undervoltage condition
    #180;
    Vin_Comp = 1'b1; // Reset condition
    
    #120;
    rst_bit = 1'b1; // Active high reset
    #100;
    rst_bit = 1'b0;
    #120;
    
    // Test Condition 4: Test Condition Triggered
    test_bit = 1'b1; // Simulate test condition
    #120;

    rst_bit = 1'b1; // Active high reset
    #20;
    rst_bit = 1'b0; // Active low reset
    #20;
    rst_bit = 1'b1;
    #20;    
    rst_bit = 1'b0;
    #20;

    test_bit = 1'b0; // Reset condition

    #120;
    rst_bit = 1'b1; // Active high reset
    #100;
    rst_bit = 1'b0;
    #120;
    
    // Finalize test
    #100;
    $stop; // End simulation
end

endmodule
