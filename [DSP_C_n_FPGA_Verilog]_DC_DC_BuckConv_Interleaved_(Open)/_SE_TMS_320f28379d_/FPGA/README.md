**Linux OS:**

1. Create a new udev rules file to set proper permissions for the USB Blaster:  
   ```bash
   sudo nano /etc/udev/rules.d/51-usbblaster.rules
   ```
   
2. Add the following lines (Vendor ID: 09fb is Altera, Product IDs may vary depending on the USB Blaster model):
   ```bash
   SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", MODE="0666"
   ```
   
   If you know the exact product IDs, you can specify them as well (for example):
   ```bash
   SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6001", MODE="0666"
   SUBSYSTEM=="usb", ATTR{idVendor}=="09fb", ATTR{idProduct}=="6010", MODE="0666"
   ```
   
3. Save the file and reload udev rules:
   ```bash
   sudo udevadm control --reload-rules
   sudo udevadm trigger
   ```
   
4. Unplug and re-plug the USB Blaster.

**After these steps, normal users can run Quartus II programming tools (e.g. `jtagconfig`, `quartus_pgm`) without needing additional privileges.**


**Necessary Steps (for Cyclone III EP3C16Q240C8):**

1. **Identify the On-Board Flash Device**  
   Typically, Cyclone III boards use EPCS (serial configuration) devices for non-volatile storage. Confirm your board’s flash (e.g., EPCS16).

2. **Convert .SOF to a .JIC File**  
   - In Quartus, go to **File > Convert Programming Files**.  
   - Set “File type” to **JTAG Indirect Configuration File (.jic)**.  
   - Under “Flash device”, select the correct EPCS device (e.g., **EPCS16**).  
   - Add your .SOF file as input.  
   - Click **Generate** to create the .jic file.

3. **Use the Quartus Programmer**  
   - Open **Tools > Programmer**.  
   - Click **Auto-Detect** to ensure the FPGA and EPCS device are recognized.  
   - Add the .jic file.  
   - Double-click the line beneath the .jic file to select the EPCS device.  
   - Check the “Program/Configure” box for the EPCS device.

4. **Program the Flash**  
   - Click **Start** to program the EPCS device.  
   - Once done, power-cycling the board will load the FPGA configuration from the flash, eliminating the need to manually reprogram the FPGA each time.