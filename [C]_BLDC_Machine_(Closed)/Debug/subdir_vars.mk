################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../28069_RAM_lnk.cmd \
../F2806x_Headers_nonBIOS.cmd 

ASM_SRCS += \
../DSP2833x_ADC_cal.asm 

C_SRCS += \
../abc_dq.c \
../dq_abc.c \
../filtr.c \
../funkcje.c \
../mainBLDC.c \
../modulator.c \
../modulator2.c \
../regulator_PI.c \
../transformation.c 

C_DEPS += \
./abc_dq.d \
./dq_abc.d \
./filtr.d \
./funkcje.d \
./mainBLDC.d \
./modulator.d \
./modulator2.d \
./regulator_PI.d \
./transformation.d 

OBJS += \
./DSP2833x_ADC_cal.obj \
./abc_dq.obj \
./dq_abc.obj \
./filtr.obj \
./funkcje.obj \
./mainBLDC.obj \
./modulator.obj \
./modulator2.obj \
./regulator_PI.obj \
./transformation.obj 

ASM_DEPS += \
./DSP2833x_ADC_cal.d 

OBJS__QUOTED += \
"DSP2833x_ADC_cal.obj" \
"abc_dq.obj" \
"dq_abc.obj" \
"filtr.obj" \
"funkcje.obj" \
"mainBLDC.obj" \
"modulator.obj" \
"modulator2.obj" \
"regulator_PI.obj" \
"transformation.obj" 

C_DEPS__QUOTED += \
"abc_dq.d" \
"dq_abc.d" \
"filtr.d" \
"funkcje.d" \
"mainBLDC.d" \
"modulator.d" \
"modulator2.d" \
"regulator_PI.d" \
"transformation.d" 

ASM_DEPS__QUOTED += \
"DSP2833x_ADC_cal.d" 

ASM_SRCS__QUOTED += \
"../DSP2833x_ADC_cal.asm" 

C_SRCS__QUOTED += \
"../abc_dq.c" \
"../dq_abc.c" \
"../filtr.c" \
"../funkcje.c" \
"../mainBLDC.c" \
"../modulator.c" \
"../modulator2.c" \
"../regulator_PI.c" \
"../transformation.c" 


