# Repository Overview

This repository contains various university and personal projects related to control engineering and other fields.

## Projects

### [C]_BLDC_Machine_(Closed)
- Project related to BLDC (Brushless DC) machines.

### [DSP_n_FPGA]_DC_DC_BuckConv_Interleaved_(Open)
- Project on interleaved DC-DC buck converters.
    - **_PE_Simulation**: Power electronics simulation.
    - **_SE_TMS_320f28379d_**: Implementation on TMS320F28379D microcontroller.
    - **_FPGA_Cyclone_III_EP3C16Q240C8_**: Implementation of over-current/voltage protection on FPGA Cyclone III EP3C16Q240C8.

### [C]_GPS_Read_(Closed)
- Project for reading GPS data.
    - **lcd.c**: Source code for LCD display.
    - **main.c**: Main source code.
    - **TMSM_Projekt_GPS_LCD.pdf**: Project documentation.

### [C]_Inverter_DRFOC_(Open)
- Development project for Direct Rotor Field Oriented Control (DRFOC) inverters.
    - **DSP_TMS320x335**: Implementation on DSP TMS320x335.
    - **PE_Simulation**: Power electronics simulation.

### [MatLab]_System_Elektroenergetyczny_MatpowerLib_(Closed)
- Project using Matpower library for electrical power systems in MATLAB.
    - **Wpływ strategii ładowania samochodów elektrycznych na napięcia w sieci SN.pdf**: Document on the impact of electric vehicle charging strategies on medium voltage network voltages.

### [Python]_AV_JetRacer_(Closed)
- Autonomous vehicle project using JetRacer.
    - **PurePursuite.py**: Pure pursuit algorithm implementation.
    - **Report.pdf**: Project report.
    - **PID_Regulator.py**: PID regulator implementation.

### [Python]_GUI_Build_(Closed)
- Project for building graphical user interfaces in Python.
    - **Eurostat_Electricity_Price_Comparison.py**: Script for comparing electricity prices using Eurostat data.
    - **Packs_n_Mods**: Additional packages and modules.
