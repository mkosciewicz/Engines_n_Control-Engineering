function additional_load_profile = generate_additional_load_profile(num_intervals)
    % Parameters for additional load
    min_load = 10000; % Minimum additional load in MW
    max_load = 20000; % Maximum additional load in MW
    duration = 2 * 4; % Duration of each additional load event (2 hours * 4 intervals per hour)
    
    % Initialize additional load profile
    additional_load_profile = zeros(1, num_intervals);
    
    % Define times for additional load events (e.g., 08:00-10:00 and 18:00-20:00)
    event1_start = 8 * 4 + 1; % Start at 08:00
    event2_start = 16 * 4 + 1; % Start at 18:00
    
    % Generate fluctuating load between min_load and max_load
    for i = 0:(duration - 1)
        additional_load_profile(event1_start + i) = min_load + (max_load - min_load) * rand();
        additional_load_profile(event2_start + i) = min_load + (max_load - min_load) * rand();
    end
end
