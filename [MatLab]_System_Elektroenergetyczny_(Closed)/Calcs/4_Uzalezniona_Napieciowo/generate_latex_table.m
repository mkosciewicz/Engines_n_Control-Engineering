function generate_latex_table(non_ev_cost, ev_cost, total_cost, total_power_used, total_evs_charged_30min, total_evs_charged_1hour, total_evs_charged_8hours)
    % Definiowanie ścieżek do podkatalogów
    output_subdir = '/home/mkoscie/PW_EE/Emob_sem_VI/IPSS/Report/data/strtg_4'; % Zmień to na ścieżkę do podkatalogu, w którym ma być zapisany plik .tex
    
    % Upewnienie się, że katalog docelowy istnieje
    if ~exist(output_subdir, 'dir')
        mkdir(output_subdir);
    end
    
    % Ścieżka do pliku wynikowego
    output_file = fullfile(output_subdir, 'stga4_costs_summary.tex');
    
    % Otwarcie pliku do zapisu kodu LaTeX
    fid = fopen(output_file, 'w');
    
    % Obliczanie sum
    total_cost_sum = sum(total_cost); % Zakładając, że koszt całkowity to suma total_cost
    total_power_use = sum(total_power_used);
    
    % Obliczanie Euro na kW
    euros_per_wh = total_cost_sum / total_power_use;
    
    % Obliczanie całkowitej liczby naładowanych EV
    total_evs_charged = total_evs_charged_30min + total_evs_charged_1hour + total_evs_charged_8hours;
    
    % Zapisywanie zawartości LaTeX
    fprintf(fid, '\\begin{table}[H]\n');
    fprintf(fid, '\\centering\n');
    fprintf(fid, '\\begin{tabular}{|c|c|}\n');
    fprintf(fid, '\\hline\n');
    fprintf(fid, 'Parametr & Wartość \\\\\n');
    fprintf(fid, '\\hline\n');
    fprintf(fid, 'Euro na Wh (€/Wh) & %.2f \\\\\n', euros_per_wh);
    fprintf(fid, 'Całkowita liczba naładowanych EV (30 min) & %d \\\\\n', total_evs_charged_30min);
    fprintf(fid, 'Całkowita liczba naładowanych EV (1 godzina) & %d \\\\\n', total_evs_charged_1hour);
    fprintf(fid, 'Całkowita liczba naładowanych EV (8 godzin) & %d \\\\\n', total_evs_charged_8hours);
    fprintf(fid, 'Całkowita liczba naładowanych EV (wszystkie scenariusze) & %d \\\\\n', total_evs_charged);
    fprintf(fid, '\\hline\n');
    fprintf(fid, '\\end{tabular}\n');
    fprintf(fid, '\\caption{Podsumowanie całkowitych kosztów i zużycia energii w niezkoordynowanej strategii}\n');
    fprintf(fid, '\\label{tab:costs_summary}\n');
    fprintf(fid, '\\end{table}\n');
    
    fclose(fid);
end
