function [total_load_profile, ev_load_profile, hourly_total_cost, ev_counts_30min, ev_counts_1hour, ev_counts_8hours, additional_load_profile] = simulate_power_usage(mpc, node_index, non_ev_load, max_available_load)
    % Number of intervals (15-minute intervals throughout the day)
    num_intervals = 24 * 4;
    intervals_per_hour = 4;
    
    % High demand intervals based on employee shift changes
    high_demand_intervals = [
        (6*4+1):(9*4), ... % 06:00-09:00
        (12*4+2):(14*4), ... % 12:30-14:00
        (16*4+1):(18*4+2), ... % 16:00-18:30
    ];

    % Night charging intervals
    night_charging_intervals = [
        (0*4+1):(8*4), ... % 00:00-06:00
        (18*4+2):(23*4+4) ... % 18:30-22:00
    ];

    % Generate non-EV load profile based on price data
    non_ev_load_profile = generate_non_ev_load_profile();

    % Generate extra power profile
    extra_power_profile = generate_extra_power_profile(num_intervals);
    
    % Generate additional load profile
    additional_load_profile = generate_additional_load_profile(num_intervals);

    % Initialize load profiles
    ev_load_profile = zeros(1, num_intervals);
    total_load_profile = non_ev_load_profile + additional_load_profile;
    ev_counts_30min = zeros(1, num_intervals); % Number of EVs for 30-minute charging
    ev_counts_1hour = zeros(1, num_intervals); % Number of EVs for 1-hour charging
    ev_counts_8hours = zeros(1, num_intervals); % Number of EVs for 8-hour charging

    % Calculate energy required for charging
    energy_required_per_ev = 41.67; % kWh

    % Fast charging scenarios
    fast_charge_time_30min = 0.5; % hours
    fast_charge_time_1hour = 1; % hours
    night_charge_time_8hours = 8; % hours
    average_power_30min = energy_required_per_ev / fast_charge_time_30min; % kW
    average_power_1hour = energy_required_per_ev / fast_charge_time_1hour; % kW
    average_power_8hours = energy_required_per_ev / night_charge_time_8hours; % kW

    % Tracking EV charging
    ev_charging_30min = zeros(1, num_intervals); % EVs charging for 30 minutes
    ev_charging_1hour = zeros(1, num_intervals); % EVs charging for 1 hour
    ev_charging_8hours = zeros(1, num_intervals); % EVs charging for 8 hours

    % Simulate EV load for high demand periods
    for i = 1:num_intervals
        % Determine available power, including extra power and additional load
        available_power = max_available_load + extra_power_profile(i) - total_load_profile(i);
        if available_power < 0
            available_power = 0; % Ensure available power is non-negative
        end

        if available_power > 0
            if ismember(i, high_demand_intervals)
                % For high demand periods, use 30-minute charging
                num_ev = min(floor(available_power / average_power_30min), 60); % Limit number of EVs to a reasonable level
                ev_counts_30min(i) = num_ev;
                for j = 0:intervals_per_hour/2-1 % 30 minutes = 2 intervals
                    if (i+j) <= num_intervals
                        ev_charging_30min(i+j) = ev_charging_30min(i+j) + num_ev;
                    end
                end
            elseif ismember(i, night_charging_intervals)
                % For night periods, use 8-hour charging
                num_ev = min(floor(available_power / average_power_8hours), 50); % Limit number of EVs to a reasonable level
                ev_counts_8hours(i) = num_ev;
                for j = 0:night_charge_time_8hours*intervals_per_hour-1
                    if (i+j) <= num_intervals
                        ev_charging_8hours(i+j) = ev_charging_8hours(i+j) + num_ev;
                    end
                end
            else
                % For normal periods, use 1-hour charging
                num_ev = min(floor(available_power / average_power_1hour), 130); % Limit number of EVs to a reasonable level
                ev_counts_1hour(i) = num_ev;
                for j = 0:intervals_per_hour-1 % 1 hour = 4 intervals
                    if (i+j) <= num_intervals
                        ev_charging_1hour(i+j) = ev_charging_1hour(i+j) + num_ev;
                    end
                end
            end
        end
    end

    % Summing EV load
    for i = 1:num_intervals
        ev_load_profile(i) = ev_charging_30min(i) * average_power_30min + ...
                             ev_charging_1hour(i) * average_power_1hour + ...
                             ev_charging_8hours(i) * average_power_8hours;
        total_load_profile(i) = non_ev_load_profile(i) + ev_load_profile(i) + additional_load_profile(i);
    end

    % Adding randomness to load profiles
    total_load_profile = total_load_profile + randn(size(total_load_profile)) * 0.5; % Add small random noise
    ev_load_profile = ev_load_profile + randn(size(ev_load_profile)) * 0.5; % Add small random noise

    % Ensure no negative values
    total_load_profile(total_load_profile < 0) = 0;
    ev_load_profile(ev_load_profile < 0) = 0;

    % Initialize hourly cost arrays
    hourly_total_cost = zeros(1, 24);

    % Calculate hourly costs based on energy usage integral
    for hour = 1:24
        start_interval = (hour - 1) * intervals_per_hour + 1;
        end_interval = hour * intervals_per_hour;

        % Calculate total power usage in the hour
        total_power_usage = total_load_profile(start_interval:end_interval);
        integral_power_usage = trapz(total_power_usage) * (1/4); % 1/4 because each interval is 15 minutes

        hourly_total_cost(hour) = integral_power_usage; % Use integral as cost for simplicity
    end
end
