clc; clear; close all;

addpath('/home/mkoscie/.Program_Files/Matlab/matpower/');
define_constants; % Funkcja MATPOWER do zdefiniowania stałych dla przypadku

mpc = loadcase('/home/mkoscie/.Program_Files/Matlab/matpower/case57.m');
node_index = find(mpc.bus(:, BUS_I) == 15);

non_ev_load = 17000; % Obciążenie niezwiązane z pojazdami elektrycznymi w kW
max_available_load = 22000; % Dostępna pojemność w węźle w kW

[total_load_profile, ev_load_profile, hourly_total_cost, ev_counts_30min, ev_counts_1hour, ev_counts_8hours, additional_load_profile] = simulate_power_usage(mpc, node_index, non_ev_load, max_available_load);

% Wykres wyników
plot_results(total_load_profile, ev_load_profile, hourly_total_cost, ev_counts_30min, ev_counts_1hour, ev_counts_8hours, additional_load_profile);

% Całkowita liczba naładowanych pojazdów elektrycznych w każdym scenariuszu
total_evs_charged_30min = sum(ev_counts_30min);
total_evs_charged_1hour = sum(ev_counts_1hour);
total_evs_charged_8hours = sum(ev_counts_8hours);

fprintf('Całkowita liczba naładowanych pojazdów elektrycznych (30 minut): %d\n', total_evs_charged_30min);
fprintf('Całkowita liczba naładowanych pojazdów elektrycznych (1 godzina): %d\n', total_evs_charged_1hour);
fprintf('Całkowita liczba naładowanych pojazdów elektrycznych (8 godzin): %d\n', total_evs_charged_8hours);


% Obliczanie całkowitej mocy zużywanej i kosztów
total_power_used = sum(total_load_profile); % Całkowita zużyta moc
total_ev_power_used = sum(ev_load_profile); % Całkowita moc zużyta przez pojazdy elektryczne
total_non_ev_power_used = total_power_used - total_ev_power_used; % Całkowita moc zużyta przez nie-EV

% Zakładając, że hourly_total_cost reprezentuje koszt na godzinę, musimy go
% rozdzielić między EV i nie-EV na podstawie ich stosunku zużywanej mocy
non_ev_cost = hourly_total_cost * (total_non_ev_power_used / total_power_used); % Koszt dla obciążenia nie-EV
ev_cost = hourly_total_cost * (total_ev_power_used / total_power_used); % Koszt dla obciążenia EV
total_evs_charged = sum(ev_counts_30min+ev_counts_1hour+ev_counts_8hours);

generate_latex_table(non_ev_cost, ev_cost, hourly_total_cost, total_load_profile, total_evs_charged_30min, total_evs_charged_1hour, total_evs_charged_8hours);
