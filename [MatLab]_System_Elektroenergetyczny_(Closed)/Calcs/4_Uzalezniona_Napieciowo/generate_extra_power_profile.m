function extra_power_profile = generate_extra_power_profile(num_intervals)
    % Parameters for Gaussian distribution
    peak_power = 50; % Peak extra power in kW
    peak_time = 12 * 4; % Peak at 12:00 (12th hour * 4 intervals per hour)
    sigma = 4 * 2; % Spread of the Gaussian (2 hours * 4 intervals per hour)

    % Time vector
    time_intervals = 1:num_intervals;

    % Gaussian distribution for extra power
    extra_power_profile = peak_power * exp(-((time_intervals - peak_time).^2) / (2 * sigma^2));
end
