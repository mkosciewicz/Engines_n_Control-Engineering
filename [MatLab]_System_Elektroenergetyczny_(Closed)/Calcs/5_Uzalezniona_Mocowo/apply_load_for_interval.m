function mpc_updated = apply_load_for_interval(mpc, load, node_index, max_ev_load)
    PD = 3; 
    % Update the load at the specified node for the given interval
    mpc_updated = mpc;
    mpc_updated.bus(node_index, PD) = load;
    % Ensure load does not exceed the max EV load capacity
    if load > max_ev_load
        mpc_updated.bus(node_index, PD) = max_ev_load;
    end
end
