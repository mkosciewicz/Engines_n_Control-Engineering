function [total_load_profile, ev_load_profile, hourly_total_cost, ev_counts_30min, ev_counts_1hour, ev_counts_8hours] = simulate_power_usage(mpc, node_index, non_ev_load, max_available_load)
    % Liczba interwałów (interwały 15-minutowe w ciągu dnia)
    num_intervals = 24 * 4;
    intervals_per_hour = 4;
    
    % Interwały wysokiego zapotrzebowania na EV na podstawie zmiany pracowników
    high_demand_intervals = [
        (6*4+1):(9*4), ... % 06:00-09:00
        (12*4+2):(14*4), ... % 12:30-14:00
        (16*4+1):(18*4+2), ... % 16:00-18:30
    ];

    % Interwały nocnego ładowania
    night_charging_intervals = [
        (0*4+1):(6*4), ... % 00:00-06:00
        (18*4+3):(23*4+4) ... % 18:30-22:00
    ];

    % Generowanie profilu obciążenia nie-EV na podstawie danych o cenach
    non_ev_load_profile = generate_non_ev_load_profile();

    % Inicjalizacja profili obciążenia
    ev_load_profile = zeros(1, num_intervals);
    total_load_profile = non_ev_load_profile;
    ev_counts_30min = zeros(1, num_intervals); % Liczba EV dla ładowania 30-minutowego
    ev_counts_1hour = zeros(1, num_intervals); % Liczba EV dla ładowania 1-godzinnego
    ev_counts_8hours = zeros(1, num_intervals); % Liczba EV dla ładowania 8-godzinnego

    % Obliczanie energii wymaganej do ładowania
    energy_required_per_ev = 41.67; % kWh

    % Scenariusze szybkiego ładowania
    fast_charge_time_30min = 0.5; % godziny
    fast_charge_time_1hour = 1; % godziny
    night_charge_time_8hours = 8; % godziny
    average_power_30min = energy_required_per_ev / fast_charge_time_30min; % kW
    average_power_1hour = energy_required_per_ev / fast_charge_time_1hour; % kW
    average_power_8hours = energy_required_per_ev / night_charge_time_8hours; % kW

    % Śledzenie ładowania EV
    ev_charging_30min = zeros(1, num_intervals); % EV ładowane przez 30 minut
    ev_charging_1hour = zeros(1, num_intervals); % EV ładowane przez 1 godzinę
    ev_charging_8hours = zeros(1, num_intervals); % EV ładowane przez 8 godzin

    % Symulowanie obciążenia EV dla okresów wysokiego zapotrzebowania
    for i = 1:num_intervals
        available_power = max_available_load - total_load_profile(i);
        
        if available_power > 0
            if ismember(i, high_demand_intervals)
                % Dla okresów wysokiego zapotrzebowania używaj ładowania 30-minutowego
                num_ev = min(floor(available_power / average_power_30min), 20); % Ogranicz liczbę EV do rozsądnego poziomu
                ev_counts_30min(i) = num_ev;
                for j = 0:intervals_per_hour/2-1 % 30 minut = 2 interwały
                    if (i+j) <= num_intervals
                        ev_charging_30min(i+j) = ev_charging_30min(i+j) + num_ev;
                    end
                end
            elseif ismember(i, night_charging_intervals)
                % Dla okresów nocnych używaj ładowania 8-godzinnego
                num_ev = min(floor(available_power / average_power_8hours), 50); % Ogranicz liczbę EV do rozsądnego poziomu
                ev_counts_8hours(i) = num_ev;
                for j = 0:night_charge_time_8hours*intervals_per_hour-1
                    if (i+j) <= num_intervals
                        ev_charging_8hours(i+j) = ev_charging_8hours(i+j) + num_ev;
                    end
                end
            else
                % Dla normalnych okresów używaj ładowania 1-godzinnego
                num_ev = min(floor(available_power / average_power_1hour), 30); % Ogranicz liczbę EV do rozsądnego poziomu
                ev_counts_1hour(i) = num_ev;
                for j = 0:intervals_per_hour-1 % 1 godzina = 4 interwały
                    if (i+j) <= num_intervals
                        ev_charging_1hour(i+j) = ev_charging_1hour(i+j) + num_ev;
                    end
                end
            end
        end
    end

    % Sumowanie obciążenia EV
    for i = 1:num_intervals
        ev_load_profile(i) = ev_charging_30min(i) * average_power_30min + ...
                             ev_charging_1hour(i) * average_power_1hour + ...
                             ev_charging_8hours(i) * average_power_8hours;
        total_load_profile(i) = non_ev_load_profile(i) + ev_load_profile(i);
    end

    % Wprowadzanie zmienności w profilach obciążenia
    total_load_profile = total_load_profile + randn(size(total_load_profile)) * 0.5; % Dodaj mały losowy szum
    ev_load_profile = ev_load_profile + randn(size(ev_load_profile)) * 0.5; % Dodaj mały losowy szum

    % Zapewnienie braku wartości ujemnych
    total_load_profile(total_load_profile < 0) = 0;
    ev_load_profile(ev_load_profile < 0) = 0;

    % Inicjalizacja tablic kosztów godzinowych
    hourly_total_cost = zeros(1, 24);

    % Obliczanie kosztów godzinowych na podstawie całki z użycia energii
    for hour = 1:24
        start_interval = (hour - 1) * intervals_per_hour + 1;
        end_interval = hour * intervals_per_hour;

        % Obliczanie całkowitej zużytej energii w ciągu godziny
        total_power_usage = total_load_profile(start_interval:end_interval);
        integral_power_usage = trapz(total_power_usage) * (1/4); % 1/4, ponieważ każdy interwał to 15 minut

        hourly_total_cost(hour) = integral_power_usage; % Używanie całki jako kosztu dla prostoty
    end
end
