function non_ev_load_profile = generate_non_ev_load_profile()
    % Przykładowe dane godzinowe dotyczące cen źródło: https://tge.pl/energia-elektryczna-rdn
    hourly_price = [
        368.150, 405.450, 366.99, 50, 50, 375.52, 371.26, 50, 339.16, 341.42, ...
        367.550, 376.73, 348.98, 368.82, 375.57, 388.37, 50, 50, 452.39, 460.92, ...
        460.92, 465.19, 448.12, 413.98
    ];

    % Znormalizuj ceny do maksymalnej wartości 17 MW
    normalized_load = hourly_price / max(hourly_price) * 17000;

    % Rozszerz do interwałów 15-minutowych
    non_ev_load_profile = repelem(normalized_load, 4);

    % Wprowadź zmienność
    non_ev_load_profile = non_ev_load_profile + randn(size(non_ev_load_profile)) * 0.7; % Dodaj mały losowy szum
    
    % Zapewnij ograniczenia
    non_ev_load_profile(non_ev_load_profile < 4) = 4;
    
end
