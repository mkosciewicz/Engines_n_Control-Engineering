# Introduction

This README file provides an overview of the Call on Teams from 2024-04-19
## Quantification

The quantification refers to the division of measurements. In this case, measurements are taken every 15 minutes.

## Creating a Demand Profile

A demand profile is a matrix that represents different capacities and energy requirements for charging. It can also include information about faster chargers that consume more power but charge for a shorter duration.

For now, let's assume the most typical chargers are 22 kilowatts. The charger has a characteristic where it doesn't consume energy during the first hour of the night and then reaches maximum consumption.

## User Consumption

There are different users with varying power consumption. Let's say there is a user with consumption X and another group of users with consumption Y.

## Tariff Group

Let's assume there is a tariff group called G, which represents residential users. The highest consumption occurs at 17:00, where the node consumes 22.11 kilowatts.

## System Load

Based on the profile, it can be estimated that the system load will be approximately half of the total power consumption.

## Charging Characteristics

The charging characteristics of electric vehicles can be adjusted to take advantage of cheaper electricity rates during specific hours.
