function plot_results(total_load_profile, ev_load_profile, hourly_total_cost, ev_charging_30min, ev_charging_1hour, ev_charging_8hours)
    % Wektor czasu dla interwałów 15-minutowych
    time_vector = (0:1/4:24-1/4);

    % Godzinowy wektor czasu dla kosztów
    hourly_time_vector = 0.5:1:23.5;

    % Interpolacja danych dla płynniejszych krzywych
    fine_time_vector = linspace(0, 24, 1000);
    total_load_interpolated = interp1(time_vector, total_load_profile / 1000, fine_time_vector, 'pchip'); % Konwersja kW na MW dla wykresu
    ev_load_interpolated = interp1(time_vector, ev_load_profile / 1000, fine_time_vector, 'pchip'); % Konwersja kW na MW dla wykresu
    non_ev_load_interpolated = interp1(time_vector, (total_load_profile - ev_load_profile) / 1000, fine_time_vector, 'pchip'); % Konwersja kW na MW dla wykresu
    cumulative_cost_interpolated = interp1(hourly_time_vector, cumsum(hourly_total_cost), fine_time_vector, 'pchip');
    ev_charging_30min_interpolated = interp1(time_vector, ev_charging_30min, fine_time_vector, 'pchip');
    ev_charging_1hour_interpolated = interp1(time_vector, ev_charging_1hour, fine_time_vector, 'pchip');
    ev_charging_8hours_interpolated = interp1(time_vector, ev_charging_8hours, fine_time_vector, 'pchip');

    % Wykres profilu całkowitego obciążenia
    figure;
    subplot(3,1,1);
    plot(fine_time_vector, total_load_interpolated, 'b-', 'LineWidth', 2);
    hold on;
    plot(fine_time_vector, ev_load_interpolated, 'r--', 'LineWidth', 2);
    plot(fine_time_vector, non_ev_load_interpolated, 'g-.', 'LineWidth', 2); % Obciążenie bez EV
    hold off;
    xlabel('Czas dnia (godziny)');
    ylabel('Obciążenie (MW)');
    legend('Całkowite obciążenie', 'Obciążenie EV', 'Obciążenie bez EV', 'Location', 'Best');
    title('Wykorzystanie mocy w ciągu dnia');
    grid on;
    ax = gca;
    ax.FontName = 'Anonymous Pro';
    ax.FontSize = 13;
    xticks(0:1:24);

    % Wykres kosztów skumulowanych
    subplot(3,1,2);
    plot(fine_time_vector, cumulative_cost_interpolated, 'k-', 'LineWidth', 2);
    xlabel('Czas dnia (godziny)');
    ylabel('Koszt skumulowany (€)');
    title('Skumulowane koszty w ciągu dnia');
    grid on;
    ax = gca;
    ax.FontName = 'Anonymous Pro';
    ax.FontSize = 13;
    xticks(0:1:24);

    % Wykres liczby podłączonych EV za pomocą wykresów liniowych
    subplot(3,1,3);
    plot(fine_time_vector, ev_charging_30min_interpolated, 'r-', 'LineWidth', 2, 'DisplayName', 'Ładowanie 30 minut');
    hold on;
    plot(fine_time_vector, ev_charging_1hour_interpolated, 'g-', 'LineWidth', 2, 'DisplayName', 'Ładowanie 1 godzina');
    plot(fine_time_vector, ev_charging_8hours_interpolated, 'b-', 'LineWidth', 2, 'DisplayName', 'Ładowanie 8 godzin');
    hold off;
    xlabel('Czas dnia (godziny)');
    ylabel('Liczba EV');
    legend('Location', 'Best');
    title('Liczba podłączonych EV');
    grid on;
    ax = gca;
    ax.FontName = 'Anonymous Pro';
    ax.FontSize = 13;
    xticks(0:1:24);
end