#include "DSP28x_Project.h"
#include "DSP2833x_Device.h"

#include "_hdrs_/tms28335_setupADC.h"
#include "_hdrs_/tms28335_setupGPIO.h"
#include "_hdrs_/tms28335_setupPWM_n_TZ.h"
#include "_hdrs_/tms28335_handleISR.h"

#include "_hdrs_/tms28335_handleCAN.h"

unsigned int mainLoopCounter = 0;
volatile int mode = 1;

int main(void) {

    InitSysCtrl();  
    setupGPIO();
    InitCpuTimers();
    DINT;

    InitPieCtrl();  
    IER = 0x0000;
    IFR = 0x0000;

    InitPieVectTable();

    enableISR();
    InitializePWMModules();
    ConfigurePWMTripZones();
    setupTimer();
    setupCAN(); 
    mailboxingCAN();
         
    setupISR();

    while (1) { 
        ControlPWMModules(mode);
        handleCANTx();
        handleCANRx();
        mainLoopCounter++; 
    }
}
