#include "DSP28x_Project.h"
#include "_hdrs_/tms28335_setupISR.h"

float elapsedTime = 0.0f;

volatile bool can_tx_flag = false;
volatile bool can_rx_flag = false;
unsigned int cntr_adc = 0U;
unsigned int cntr_can = 0U;

int CloseLoop = 0;
int TZ_RESET = 0;
float slip = 0.0f;
float calc_dcVoltage = 0.0f;
int pwm_latch_off = 0;

float alpha = 0.0f;
float amplitude = 1.0f;
float TWO_PI = 6.283185307179586f;

unsigned int index_startUp = 0;
unsigned int index_Operatio = 0;

#pragma DATA_SECTION(eAngle,     "BuffersDataL3");
float32 eAngle [850];
unsigned int index = 0;

GLOBAL_VARIABLES_STRUCT gVars;
DISCRETE_TRANSFER_FUNC_STRUCT estimator_rotorFlux, ref_speed_tranFunc, meas_speed_tranFunc, voltage_lpf;
ESTIMATOR_STRUCT estimator_electricAngle;
QEP_STRUCT measRotorSpeed;
PI_STRUCT Pi_id, Pi_iq, Pi_speed, Pi_flux;
ABC_DQ_ABC_STRUCT measStatorCurrent, refStatorVoltage_alfabeta, refStatorVoltage_abc;
LIMITER_STRUCT result;
MODULATOR_STRUCT modulator;

void setupISR() {
    DINT;

    InitPieCtrl();

    IER = 0x0000;
    IFR = 0x0000;

    InitPieVectTable();

    EALLOW;
    // PieVectTable.ECAN0INTA = &CAN_ISR;
    PieVectTable.SEQ1INT = &ADC_ISR;
    PieVectTable.TINT0 = &CAN_Tx_TIMER_ISR;
    PieVectTable.XINT13 = &CAN_Rx_TIMER_ISR;
    
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
    PieCtrlRegs.PIEIER1.bit.INTx1 = 1; //Sequencer
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1; // Timer0

    PieCtrlRegs.PIEIFR1.all = 0;
    IER |= M_INT1 | M_INT13;
    
    EINT;
    ERTM; 

    GlobalVariables_Init(&gVars);
    Parameters_Calc(&gVars);
   
	DiscreteTF_Init(&estimator_rotorFlux, gVars.fluxEstimator.num_coeffs, 0, gVars.fluxEstimator.den_coeffs, 1);
	DiscreteTF_Init(&ref_speed_tranFunc, gVars.ref_speed.num_coeffs, 0, gVars.ref_speed.den_coeffs, 1);
	DiscreteTF_Init(&meas_speed_tranFunc, gVars.meas_speed.num_coeffs, 0, gVars.meas_speed.den_coeffs, 1);
	DiscreteTF_Init(&voltage_lpf, gVars.voltage_lpf.num_coeffs, 0, gVars.voltage_lpf.den_coeffs, 1);
    setupQEP();
    initQEP(&measRotorSpeed);

    ElectroTheta_Estimator_Init(&estimator_electricAngle, gVars.Ts, gVars.magnetizing_Inductance, gVars.rotor_Tau, gVars.ref_rotorFlux_rated, gVars.poles_pairs_nr);
   
    Pi_Init(&Pi_flux, gVars.Ts, gVars.KP_FLUX_CTRL, gVars.KI_FLUX_CTRL, gVars.KB_FLUX_CTRL, true, true);
    Pi_Init(&Pi_speed, gVars.Ts, gVars.KP_SPEED_CTRL, gVars.KI_SPEED_CTRL, gVars.KB_SPEED_CTRL, true, true);
    
    setupADC();
    ABC_dq_ABC_Init(&measStatorCurrent);

    Pi_Init(&Pi_id, gVars.Ts, gVars.KP_CURR_CTRL, gVars.KI_CURR_CTRL, gVars.KB_CURR_CTRL, true, true);
    Pi_Init(&Pi_iq, gVars.Ts, gVars.KP_CURR_CTRL, gVars.KI_CURR_CTRL, gVars.KB_CURR_CTRL, true, true);

    ABC_dq_ABC_Init(&refStatorVoltage_abc);
	Modulator_init(&modulator);
	GpioDataRegs.GPACLEAR.bit.GPIO7 = 1;
    EDIS;
}

//==================
// CAN_ISR
//==================
__interrupt void CAN_Tx_TIMER_ISR(){
    CpuTimer0Regs.TCR.bit.TIF = 1;
    can_tx_flag = true;
    PieCtrlRegs.PIEACK.bit.ACK1=1;

}

__interrupt void CAN_Rx_TIMER_ISR(){

    CpuTimer1Regs.TCR.bit.TIF = 1;
    can_rx_flag = true;
    PieCtrlRegs.PIEACK.bit.ACK1 = 1;
    
    cntr_can++;
}

//================
// ADC_ISR
//================

__interrupt void ADC_ISR(){

    elapsedTime += 0.0001f;

    EALLOW;

    gVars.meas_statorCurrent_a = (float)(AdcMirror.ADCRESULT0 * 0.0045f) - 10.341f;
    gVars.meas_statorCurrent_b = (float)(AdcMirror.ADCRESULT1 * 0.0046f) - 10.61f;
    gVars.meas_statorCurrent_c = (float)(AdcMirror.ADCRESULT2 * 0.0046f) - 10.556f;
    gVars.V_dcLink = (float)(AdcMirror.ADCRESULT3 * 0.0913f) - 0.4333f;

    gVars.V_dcLink_minLimit = -gVars.V_dcLink * gVars.V_dcLink_scalar;
    gVars.V_dcLink_maxLimit = gVars.V_dcLink * gVars.V_dcLink_scalar;


    // ###_SO_OCP_Check_###
    if (elapsedTime > 5.0f && !pwm_latch_off) {
        if (fabsf(gVars.meas_statorCurrent_a) > 4.5f || fabsf(gVars.meas_statorCurrent_b) > 4.5f || fabsf(gVars.meas_statorCurrent_c) > 4.5f) {
            EPwm1Regs.TZFRC.bit.OST = 1;
            EPwm2Regs.TZFRC.bit.OST = 1;
            EPwm3Regs.TZFRC.bit.OST = 1; 
            pwm_latch_off = 1;
            GpioDataRegs.GPCSET.bit.GPIO87 = 1;
        }
    }

    if (TZ_RESET) {
        pwm_latch_off = 0;
        PwmTripZoneControl(PWM_TZ_RESET);  
        TZ_RESET = 0;
    }


    //%%%%%_SO_Speed PI Regulator_%%%%%
    calculateRotorSpeed(&measRotorSpeed);
    gVars.meas_rotorSpeed = DiscreteTF_Update(&meas_speed_tranFunc,(float) measRotorSpeed.speedRadPerSec);
    // ---
    gVars.ref_rotorSpeed = DiscreteTF_Update(&ref_speed_tranFunc, gVars.set_rotorSpeed);

    Pi_Calc(&Pi_speed, gVars.ref_rotorSpeed, gVars.meas_rotorSpeed, -gVars.I_MAX, gVars.I_MAX);
    gVars.ref_statorCurrent_q = Pi_speed.out;
    //%%%%%_EO_Speed PI Regulator_%%%%%   
    
    //%%%%%_SO_Flux_PI_Regulator_n_Estimator%%%%%
    float meas_statorCurrent_d = (gVars.ref_statorCurrent_d - Pi_id.error);
    gVars.estima_rotorFlux = DiscreteTF_Update(&estimator_rotorFlux, meas_statorCurrent_d);

    float meas_statorCurrent_q = (gVars.ref_statorCurrent_q - Pi_iq.error);
    gVars.estima_theta_electro = ElectroTheta_Estimator_Update(&estimator_electricAngle, gVars.estima_rotorFlux, meas_statorCurrent_q, gVars.meas_rotorSpeed);

    Pi_Calc(&Pi_flux, gVars.ref_rotorFlux_rated, gVars.estima_rotorFlux, -gVars.I_MAX, gVars.I_MAX);
    gVars.ref_statorCurrent_d = Pi_flux.out;
    //%%%%%_EO_Flux_PI_Regulator_n_Estimator%%%%%

    abc2dq(&measStatorCurrent, gVars.meas_statorCurrent_a, gVars.meas_statorCurrent_b, gVars.meas_statorCurrent_c, gVars.estima_theta_electro);
    
    gVars.meas_statorCurrent_d = measStatorCurrent.d_axis;
    gVars.meas_statorCurrent_q = measStatorCurrent.q_axis;

    Pi_Calc(&Pi_id, gVars.ref_statorCurrent_d, measStatorCurrent.d_axis, gVars.V_dcLink_minLimit, gVars.V_dcLink_maxLimit);    
    Pi_Calc(&Pi_iq, gVars.ref_statorCurrent_q, measStatorCurrent.q_axis, gVars.V_dcLink_minLimit, gVars.V_dcLink_maxLimit);
    
    calculateLimiter(&result, Pi_id.out, Pi_iq.out, gVars.V_dcLink_maxLimit);
    gVars.ref_statorVoltage_d = result.ref_statorVoltage_d;
    gVars.ref_statorVoltage_q = result.ref_statorVoltage_q;

    dq2abc(&refStatorVoltage_abc, result.ref_statorVoltage_d, result.ref_statorVoltage_q, gVars.estima_theta_electro);

    slip = 0.01f * (gVars.omega_e - gVars.meas_rotorSpeed)/gVars.omega_e;
    
    if (gVars.V_dcLink >= 20.0f && elapsedTime >= 5.0f && slip < 0.01f) {

        if (elapsedTime > 10.0f){
            calc_dcVoltage = 30.0f + 1.732050808f * sqrtf(result.ref_statorVoltage_d * result.ref_statorVoltage_d + result.ref_statorVoltage_q * result.ref_statorVoltage_q);
        } else {
            calc_dcVoltage = gVars.V_dcLink;
        }

        gVars.request_DC_Link_Vltg = DiscreteTF_Update(&voltage_lpf, calc_dcVoltage);

        Modulator_calc(&modulator, refStatorVoltage_abc.a_phs, refStatorVoltage_abc.b_phs, refStatorVoltage_abc.c_phs, gVars.V_dcLink);

        CloseLoop = 1;
    } else {
        gVars.request_DC_Link_Vltg = gVars.V_dcLink;

        alpha = alpha + TWO_PI * gVars.statorCurrent_freq * 0.0001;
        if (alpha > TWO_PI) alpha -= TWO_PI;

        float phs_a = amplitude * sinf(alpha);
        float phs_b = amplitude * sinf(alpha - 2.094395102);
        float phs_c = amplitude * sinf(alpha + 2.094395102);

        Modulator_calc(&modulator, phs_a, phs_b, phs_c, 1.0f);
    }

    EPwm1Regs.CMPA.half.CMPA = modulator.y1 * 1250;
    EPwm2Regs.CMPA.half.CMPA = modulator.y2 * 1250;
    EPwm3Regs.CMPA.half.CMPA = modulator.y3 * 1250;

    AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;
    AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;
    AdcRegs.ADCST.bit.INT_SEQ2_CLR = 1;

    PieCtrlRegs.PIEACK.bit.ACK1=1;

    if (index < 850) {
        eAngle[index] = gVars.estima_theta_electro;
        index++;
    } 

    cntr_adc++;
    EDIS;
}
// ================== EO_ISR_Defs ========================

// ######_EOF_######

