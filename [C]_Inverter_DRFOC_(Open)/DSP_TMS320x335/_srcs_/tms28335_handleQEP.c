#include "DSP28x_Project.h"
#include "_hdrs_/tms28335_handleQEP.h"

extern float PI;

void setupQEP() {

    EALLOW;

    // Quadrature-decoding mode, no swapping/polarity
    EQep1Regs.QDECCTL.bit.QSRC = 0;  
    EQep1Regs.QDECCTL.bit.QAP  = 0;  
    EQep1Regs.QDECCTL.bit.SWAP = 0;
    
    // Position counter free-run on emulator suspend
    EQep1Regs.QEPCTL.bit.FREE_SOFT = 2; 
    // Reset pos counter on index event or other as needed
    EQep1Regs.QEPCTL.bit.PCRM      = 0; 
    EQep1Regs.QEPCTL.bit.WDE       = 0;

    // timeout for 'no-edge' detection
    EQep1Regs.QEPCTL.bit.UTE = 1;
    
    // Unit Position Prescalers
    //   CCPS = 5 (/32) 
    //   UPPS = 3 (8 edges)
    EQep1Regs.QCAPCTL.bit.CCPS = 5;//5;
    EQep1Regs.QCAPCTL.bit.UPPS = 3;     
    
    EQep1Regs.QPOSMAX = 9999;

    EQep1Regs.QCAPCTL.bit.CEN = 1;  
    EQep1Regs.QEPCTL.bit.QPEN = 1;  
    EQep1Regs.QPOSCNT = 0;
    EDIS;
}

void initQEP(QEP_STRUCT* rotor) {
    rotor->directionRotor  = 0;
    // (SysClk=150MHz, CCPS=5 => /32, UPPS=3 =>8 edges, edges/rev=10000)
    // => scaling = 3750 for revs/sec.
    rotor->scaling         =  3750.0f; //3750.0f;
    rotor->speedRadPerSec  = 0.0f;
    // rotor->speedRadPerSec_prev  = 0.0f;
}

void calculateRotorSpeed(QEP_STRUCT* rotor) {

    volatile Uint16 prdA, prdB;
    do{prdA = EQep1Regs.QPOSCNT;
    prdB = EQep1Regs.QPOSCNT;}
    while(prdA != prdB);

    if (EQep1Regs.QFLG.bit.IEL == 1) {  //If event latch
        EQep1Regs.QCLR.bit.IEL = 1;     //Clear interrupt flag
    }

    if(EQep1Regs.QEPSTS.bit.UPEVNT == 1)
    {
        Uint16 prdA, prdB;
        do {
            prdA = EQep1Regs.QCPRD;
            prdB = EQep1Regs.QCPRD;
        } while(prdA != prdB);

        if(prdA == 0)
        {
            rotor->speedRadPerSec = rotor->speedRadPerSec_prev;
        }
        else
        {
            float revPerSec = rotor->scaling / (float)prdA;

            if(EQep1Regs.QEPSTS.bit.QDF == 1) // reverse
                revPerSec = -revPerSec;

            rotor->speedRadPerSec = revPerSec * 2.0f * (float)M_PI;
        }

        // Check overflow => means QCPRD > 65535 or no edges in time
        if(EQep1Regs.QEPSTS.bit.COEF == 1)
        {
            rotor->speedRadPerSec = 0.0f;
            EQep1Regs.QEPSTS.bit.COEF = 0;
        }
        EQep1Regs.QEPSTS.all = 0x88;

        // rotor->speedRadPerSec_prev = rotor->speedRadPerSec;

    }
}
