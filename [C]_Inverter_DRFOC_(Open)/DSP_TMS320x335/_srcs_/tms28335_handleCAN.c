#include "DSP28x_Project.h"
#include "_hdrs_/tms28335_handleCAN.h"

CAN_TX_MESSAGE_STRUCT txMessage;
CAN_RX_MESSAGE_STRUCT rxMessage;

static void CanConvertFloatToBytes(uint16_t *destStartAddress, volatile float *inputData, float scaling, float offset)
{
    volatile float  tempVal;
    volatile uint16_t tempUint;

#ifdef CCS
    DINT;
#endif
    tempVal = *inputData;
#ifdef CCS
    EINT;
#endif

    // Convert float -> scaled integer
    tempUint = (uint16_t)((tempVal * scaling) + 0.0f);

    // Store in big-endian format (high byte first)
    destStartAddress[0] = (tempUint & 0xFF00) >> 8;
    destStartAddress[1] = (tempUint & 0x00FF);
}

//=============================================================
//  Data Preparation for CAN Transmission
//=============================================================
float dataCANTx(CAN_TX_MESSAGE_STRUCT *msg, uint32_t canID)
{
    msg->CanID = canID;
    memset(msg->data, 0.0f, sizeof(msg->data));  // OFFSET = 0.0f

    switch (canID) {
        case 0x01: 
            CanConvertFloatToBytes((uint16_t*)&msg->data[0], &gVars.request_DC_Link_Vltg, 1.0f, 0.0f);
           CanConvertFloatToBytes((uint16_t*)&msg->data[2], &gVars.meas_rotorSpeed, 1.0f, 0.0f);
            break;
        default:
            return -1; 
    }
    return 0; 
}

//=============================================================
//  Data Preparation for CAN Reception
//=============================================================

// *** B2F ***
static float CanConvertBytesToFloat(uint16_t *srcStartAddress, float scaling, float offset)
{
    volatile float    tempVal;
    volatile uint16_t tempHighByte;
    volatile uint16_t tempLowByte;
    volatile uint16_t tempCompleteDataInt;

#ifdef CCS
    DINT;
#endif
    tempHighByte = srcStartAddress[0];
    tempLowByte  = srcStartAddress[1];
#ifdef CCS
    EINT;
#endif

    tempCompleteDataInt = (tempHighByte << 8) | tempLowByte;
    tempVal             = (tempCompleteDataInt * scaling) - offset;
    return tempVal;
}

float dataCANRx(CAN_RX_MESSAGE_STRUCT *msg, uint32_t CanID) {
    CAN_MessageRead(msg); 
    msg->CanID = CanID;
    
    if (!msg->newDataAvailable) {
        return -1;
    }

    switch (CanID) {
        case 0x05:
            if (msg->data[4] == 0x01) {
                gVars.set_rotorSpeed = CanConvertBytesToFloat((uint16_t*)&msg->data[0], 1.0f, 0.0f);
            }
            else if (msg->data[4] == 0x00){
                gVars.set_rotorSpeed = -1.0f * CanConvertBytesToFloat((uint16_t*)&msg->data[0], 1.0f, 0.0f); 
            }
            enableLoad(msg->data[2]);
            break;
        default:
            break;
    }

    msg->newDataAvailable = 0;
    return 0;
}

void mailboxingCAN(void)
{   
    CAN_MailBoxTxSetAndInit(&txMessage, CAN_11BIT, 0x01, 4);
    txMessage.data[0] = 0x01;
    txMessage.data[1] = 0x02;
    txMessage.data[2] = 0x03;
    txMessage.data[3] = 0x04;
    txMessage.data[4] = 0xAA;
    txMessage.data[5] = 0xBB;
    txMessage.data[6] = 0xCC;
    txMessage.data[7] = 0xDD;
    CAN_MailBoxRxSetAndInit(&rxMessage, CAN_11BIT, 0x05, 5, 0x7FF);
}

void handleCAN(void)
{
    if (can_tx_flag)
    {
        dataCANTx(&txMessage, 0x01);
        CAN_MessageSend(&txMessage);
        can_tx_flag = false;
    }

    if (can_rx_flag)
    {
        dataCANRx(&rxMessage, 0x05);
        CAN_MessageRead(&rxMessage);
        can_rx_flag = false;
    }
}

void enableLoad(uint16_t value)
{
    GpioDataRegs.GPASET.bit.GPIO22 = (value == 1);
    GpioDataRegs.GPACLEAR.bit.GPIO22 = (value != 1);
}


//=========== EOF ===========
