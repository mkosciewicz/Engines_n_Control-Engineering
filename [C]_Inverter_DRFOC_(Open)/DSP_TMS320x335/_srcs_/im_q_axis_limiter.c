#include "DSP28x_Project.h"

#include "_hdrs_/im_q_axis_limiter.h"

float min_max(float input1, float input2, Operation op) {
    if (op == MIN) {
        return (input1 < input2) ? input1 : input2;
    }
    else {
        return (input1 > input2) ? input1 : input2;
    }
}

void calculateLimiter(LIMITER_STRUCT* result, float currentPI_output_d, float currentPI_output_q, float max_constant) {

    // Saturation
    if (currentPI_output_d > max_constant) {
        currentPI_output_d = max_constant;
    }
    else if (currentPI_output_d < -max_constant) {
        currentPI_output_d = -max_constant;
    }

    float currentPI_output_d_squared = currentPI_output_d * currentPI_output_d;
    float max_constant_squared = max_constant * max_constant;

    // Compute the magnitude (sqrt of the sum of squares)
    float magnitude = sqrtf(max_constant_squared - currentPI_output_d_squared);

    currentPI_output_q = min_max(magnitude * -1.0f, min_max(magnitude, currentPI_output_q, MIN), MAX);

    result->ref_statorVoltage_d = currentPI_output_d;
    result->ref_statorVoltage_q = currentPI_output_q;

}
