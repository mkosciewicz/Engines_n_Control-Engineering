#include "DSP28x_Project.h"
#include "DSP2833x_Device.h"

#include "_hdrs_/tms28335_handleISR.h"

volatile bool can_tx_flag, can_rx_flag = true;
unsigned int cntr_adc = 0;
int pwm_latch_off;

extern GLOBAL_VARIABLES_STRUCT gVars;
extern DISCRETE_TRANSFER_FUNC_STRUCT estimator_rotorFlux, ref_speed_tranFunc, meas_speed_tranFunc;
extern ESTIMATOR_STRUCT estimator_electricAngle;
extern QEP_STRUCT measRotorSpeed;
extern PI_STRUCT Pi_id, Pi_iq, Pi_speed, Pi_flux;
extern ABC_DQ_ABC_STRUCT measStatorCurrent, refStatorVoltage;
extern LIMITER_STRUCT result;
extern MODULATOR_STRUCT modulator;
extern CAN_TX_MESSAGE_STRUCT txMessage;
extern CAN_RX_MESSAGE_STRUCT rxMessage;

void enableISR() {
    EALLOW;
    PieVectTable.ECAN0INTA = &CANA_interrupt;
    PieVectTable.SEQ1INT = &handleISR;
    PieVectTable.TINT0 = &timerISR;
    EDIS;
}
// ================== SO_ISR_Defs ========================
int txTimer = 0;
int rxTimer = 0;

__interrupt void timerISR(){
    txTimer = 1;
    rxTimer = 1;
}

//================
// __interrupt CAN
//================
uint32_t canISRCounter1 = 0;
uint32_t canISRCounter2 = 0;
__interrupt void CANA_interrupt(void)
{   
    EALLOW;
    ECanaRegs.CANRMP.all = 0xFFFFFFFF; // Clear receive message pending flags
    ECanaRegs.CANTA.all  = 0xFFFFFFFF; // Clear transmit acknowledgment flags
    ECanaRegs.CANGIF0.all = 0xFFFFFFFF;
    ECanaRegs.CANGIF1.all = 0xFFFFFFFF;
    canISRCounter1++; 
    uint32_t rxPending = ECanaRegs.CANRMP.all; // Pending RX messages
    uint32_t txAck     = ECanaRegs.CANTA.all; // TX acknowledgments

    // Handle RX messages
    if (rxMessage.IntMsgNum < 32 && (rxPending & (1UL << rxMessage.IntMsgNum)))
    {
        CAN_MessageRead(&rxMessage); // Process received message
        rxMessage.newDataAvailable = 1; // Set flag for RX processing
        can_rx_flag = true;            // Signal RX event
    }

    // Handle TX acknowledgments
    if (txMessage.IntMsgNum < 32 && (txAck & (1UL << txMessage.IntMsgNum)))
    {
        txMessage.MessageCounter++; // Increment TX counter
        msgCount++;                 // Update global TX message count
        can_tx_flag = true;         // Signal TX completion
    }

    // Clear pending RX and TX flags
    ECanaRegs.CANRMP.all = (1UL << rxMessage.IntMsgNum);
    ECanaRegs.CANTA.all  = (1UL << txMessage.IntMsgNum);
    ECanaRegs.CANGIF0.all = 0xFFFFFFFF;
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP9; 
    canISRCounter2++;
    EDIS;
    
}

//================
// __interrupt ADC
//================
__interrupt void handleISR(){
    EALLOW;

    //%%%%%_SO_Speed PI Regulator_%%%%%
    float meas_rotorSpeed = (float)measRotorSpeed.speedRadPerSec;
    // ---
    gVars.ref_mechSpeed_rps = 1.0f;
    float ref_rotorSpeed = DiscreteTF_Update(&ref_speed_tranFunc, gVars.ref_mechSpeed_rps);

    Pi_Calc(&Pi_speed, ref_rotorSpeed, meas_rotorSpeed, gVars.KB_SPEED_CTRL);
    gVars.ref_statorCurrent_q_prev = gVars.ref_statorCurrent_q;
    gVars.ref_statorCurrent_q = Pi_speed.out;
    //%%%%%_EO_Speed PI Regulator_%%%%%   
    
    //%%%%%_SO_Flux_PI_Regulator_n_Estimator%%%%%
    // **Flux PI Regulator**
    Pi_Calc(&Pi_flux, gVars.ref_rotorFlux, gVars.estima_rotorFlux, gVars.KB_FLUX_CTRL);
    gVars.ref_statorCurrent_d = Pi_flux.out;

    float min_abs_value = 0.1f;
    float ref_statorCurrent_d_nonzero = (fabsf(gVars.ref_statorCurrent_d) < min_abs_value) ? copysignf(min_abs_value, gVars.ref_statorCurrent_d) : gVars.ref_statorCurrent_d; // Check Jira for explenatio

    gVars.estima_rotorFlux = DiscreteTF_Update(&estimator_rotorFlux, ref_statorCurrent_d_nonzero);
    gVars.estima_theta_electro = ElectroTheta_Estimator_Update(&estimator_electricAngle, gVars.estima_rotorFlux, gVars.ref_statorCurrent_q, meas_rotorSpeed);

    //%%%%%_EO_Flux_PI_Regulator_n_Estimator%%%%%
    gVars.meas_statorCurrent_a = AdcMirror.ADCRESULT0;
    gVars.meas_statorCurrent_b = AdcMirror.ADCRESULT1;
    gVars.meas_statorCurrent_c = AdcMirror.ADCRESULT2;
    
    //---
    gVars.calibra_statorCurrent_c = (gVars.meas_statorCurrent_c * 0.0046f) - 10.556f;
    gVars.calibra_statorCurrent_b = (gVars.meas_statorCurrent_b * 0.0046f) - 10.61f;
    gVars.calibra_statorCurrent_a = (gVars.meas_statorCurrent_a * 0.0045f) - 10.341f;

   // Check if reset is requested by external user
    if (GpioDataRegs.GPASET.bit.GPIO21) {
        pwm_latch_off = 0;
        ControlPWMModules(1);
        GpioDataRegs.GPACLEAR.bit.GPIO21 = 1; // Clear the reset request
    }

    // Check for overcurrent of I > 5.0 [A], if PWM modules are not latched off
    if (!pwm_latch_off) { // pwm_latch_off to 1, the outer condition if (!pwm_latch_off) will be false
        if (fabsf(gVars.calibra_statorCurrent_a) > 5.0f ||
            fabsf(gVars.calibra_statorCurrent_b) > 5.0f ||
            fabsf(gVars.calibra_statorCurrent_c) > 5.0f) {
            ControlPWMModules(0);
            pwm_latch_off = 1;
        }
    }

    gVars.calibra_DC_Link = (gVars.V_dcLink * 0.0913f) - 0.4333f;

    abc2dq(&measStatorCurrent, gVars.meas_statorCurrent_a, gVars.meas_statorCurrent_b, gVars.meas_statorCurrent_c, gVars.estima_theta_electro);
    float meas_statorCurrent_d = measStatorCurrent.d_axis;
    float meas_statorCurrent_q = measStatorCurrent.q_axis;

    Pi_Calc(&Pi_id, gVars.ref_statorCurrent_d, meas_statorCurrent_d, gVars.KB_CURR_CTRL);    
    Pi_Calc(&Pi_iq, gVars.ref_statorCurrent_q, meas_statorCurrent_q, gVars.KB_CURR_CTRL);

    calculateLimiter(&result, Pi_id.out, Pi_iq.out, gVars.q_axis_limit);
    gVars.ref_statorVoltage_d = result.ref_statorVoltage_d;
    gVars.ref_statorVoltage_q = result.ref_statorVoltage_q;

    dq2abc(&refStatorVoltage, result.ref_statorVoltage_d, result.ref_statorVoltage_d, gVars.estima_theta_electro);

    gVars.V_dcLink = AdcMirror.ADCRESULT3;

    gVars.V_dcLink_minLimit = -gVars.V_dcLink * gVars.V_dcLink_scalar; // V_dcLink_scalar = 0.577350269f;
    gVars.V_dcLink_maxLimit = gVars.V_dcLink * gVars.V_dcLink_scalar;

    float modulator_scalar = 1.0f / gVars.V_dcLink_maxLimit;

	refStatorVoltage.a_phs *= modulator_scalar;
	refStatorVoltage.b_phs *= modulator_scalar;
	refStatorVoltage.c_phs *= modulator_scalar;

    // Calculate modulation signals
    float signalA = ((1.0f + refStatorVoltage.a_phs) * 0.5f);
    float signalB = ((1.0f + refStatorVoltage.b_phs) * 0.5f);
    float signalC = ((1.0f + refStatorVoltage.c_phs) * 0.5f);

    Modulator_calc(&modulator, signalA, signalB, signalC);

    EPwm1Regs.CMPA.half.CMPA = modulator.y1 * 1250;
    EPwm2Regs.CMPA.half.CMPA = modulator.y2 * 1250;
    EPwm3Regs.CMPA.half.CMPA = modulator.y3 * 1250;

    AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;
    AdcRegs.ADCST.bit.INT_SEQ2_CLR = 1;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;

    cntr_adc++;
    EDIS;
}
// ================== EO_ISR_Defs ========================

// ######_EOF_######
