#include "DSP28x_Project.h"
#include "_hdrs_/tms28335_setupCAN.h"

uint16_t n = 0;

extern CAN_TX_MESSAGE_STRUCT txMessage;
extern CAN_RX_MESSAGE_STRUCT rxMessage;

void setupCAN(void){
    EALLOW;
    CAN_PeripheralModuleInit();
    setupTIMER();
    EDIS;
}

void CAN_PeripheralModuleInit()
{
    struct ECAN_REGS ECanaShadow;
    union CANMC_REG   CANMC_shadow;

    EALLOW;
    // Enable the eCAN clock
    SysCtrlRegs.PCLKCR0.bit.ECANAENCLK = 1;

    // Configure eCAN pins
    ECanaShadow.CANTIOC.all = ECanaRegs.CANTIOC.all;
    ECanaShadow.CANTIOC.bit.TXFUNC = 1;
    ECanaRegs.CANTIOC.all = ECanaShadow.CANTIOC.all;

    ECanaShadow.CANRIOC.all = ECanaRegs.CANRIOC.all;
    ECanaShadow.CANRIOC.bit.RXFUNC = 1;
    ECanaRegs.CANRIOC.all = ECanaShadow.CANRIOC.all;

    // High-end CAN mode (HECC)
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.SCB = 1;  // HECC mode
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

    // Enter config mode to set up bit timing - shadow registers to ensure atomic updates
    
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CCR = 1;        // Request config mode
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

    // (Optional) enable Automatic Bus-On (ABO)
    CANMC_shadow.all=ECanaRegs.CANMC.all;
    CANMC_shadow.bit.ABO=1;
    ECanaRegs.CANMC.all=CANMC_shadow.all;

    ECanaShadow.CANES.all = ECanaRegs.CANES.all;

    // Wait until the config change is acknowledged
    do {
        ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 1);

    // Set the desired bit rate
    CAN_BitrateSet();

    // Exit config mode
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CCR = 0;
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
    
    do {
        ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    } while(ECanaShadow.CANES.bit.CCE != 0);

    // Clear all mailboxes
    CAN_clearMailBoxes();
    
    EDIS;

}

//==========================================================
// CAN_BitrateSet - function to set the bit timing registers
//==========================================================
/* 
%%% SO_Legend %%%
BRP - Bit Configuration Parameters 
TSEG - Time segment
SJW - Synchronization Jump Width - TSEG1 can be lengthened or TSEG2 shortened by the SJW
SAM - Triple Sample
%%% EO_Legend %%%
*/

uint32_t CAN_BitrateSet()
{
    struct ECAN_REGS ECanaShadow;   
    // 500 kbps, for a 200 MHz device
    // eCAN runs at half of CPU clock -> check docs

    ECanaShadow.CANBTC.all             = 0;
    ECanaShadow.CANBTC.bit.BRPREG      = 9;  // prescaler

    // Sample point at 80% of bit time
    ECanaShadow.CANBTC.bit.TSEG1REG    = 10; 
    ECanaShadow.CANBTC.bit.TSEG2REG    = 2;

    ECanaShadow.CANBTC.bit.SAM         = 1;  // Triple sample (optional)
    ECanaRegs.CANBTC.all = ECanaShadow.CANBTC.all;

    return 0;
}

//============================================================================
// CAN_clearMailBoxes() - Clears mailbox control registers, TX/RX pending bits, etc.
//============================================================================
/*
%%% SO_Legend %%%
TA - Transmission-Acknowledge
RMP - Received-Message-Pending
GIF - Global_Interrupt_Flag
ME - Mailbox Enable
MD - Mailbox Direction
%%% EO_Legend %%%
*/
void CAN_clearMailBoxes(void)
{
    int i;
    volatile struct MBOX *mbox = &ECanaMboxes.MBOX0;
    
    EALLOW;
    for(i = 0; i < 32; i++)
        mbox[i].MSGCTRL.all = 0;
    
    ECanaRegs.CANTA.all   = 0xFFFFFFFF;
    ECanaRegs.CANRMP.all  = 0xFFFFFFFF;
    ECanaRegs.CANGIF0.all = 0xFFFFFFFF;
    ECanaRegs.CANGIF1.all = 0xFFFFFFFF;
    
    ECanaRegs.CANME.all = 0;
    EDIS;
}
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// EO_Funcs used only in setupCAN.c
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// SO_Funcs used only in handleCAN.c
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

///============================================================================
// CAN_MailBoxTxSetAndInit - Configure mailbox for TX;  Direction -> MD=0
//============================================================================
void CAN_MailBoxTxSetAndInit(CAN_TX_MESSAGE_STRUCT *m, CAN_IDTYPE Tx_MAILBOX_TYPE, uint32_t Tx_MAILBOX_ID, uint16_t Tx_MAILBOX_LENGTH)
{
    m->CAN_IdType = Tx_MAILBOX_TYPE;
    m->CanID = Tx_MAILBOX_ID;    
    m->length = Tx_MAILBOX_LENGTH;

    struct ECAN_REGS ECanaShadow;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME1 = 0;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;

    ECanaShadow.CANMD.all = ECanaRegs.CANMD.all;
    ECanaShadow.CANMD.bit.MD1 = 0;
    ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;
    
    EALLOW;
    volatile struct MBOX *p = &ECanaMboxes.MBOX1;
    if (Tx_MAILBOX_TYPE == CAN_11BIT) {
        p->MSGID.bit.IDE = 0;
        p->MSGID.bit.STDMSGID = Tx_MAILBOX_ID;
    } else {
        p->MSGID.bit.IDE = 1;
        p->MSGID.all = (Tx_MAILBOX_ID & 0x1FFFFFFF);
    }
    p->MSGCTRL.bit.DLC = Tx_MAILBOX_LENGTH;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME1 = 1;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    EDIS;
}

//========================================================================
// CAN_MessageSend Load TX mailbox and trigger transmission
//========================================================================
void CAN_MessageSend(CAN_TX_MESSAGE_STRUCT *m)
{
    volatile struct MBOX *p = &ECanaMboxes.MBOX1;

    p->MDL.byte.BYTE0 = m->data[0];
    p->MDL.byte.BYTE1 = m->data[1];
    p->MDL.byte.BYTE2 = m->data[2];
    p->MDL.byte.BYTE3 = m->data[3];
    p->MDH.byte.BYTE4 = m->data[4];
    p->MDH.byte.BYTE5 = m->data[5];
    p->MDH.byte.BYTE6 = m->data[6];
    p->MDH.byte.BYTE7 = m->data[7];

    ECanaRegs.CANTRS.bit.TRS1 = 1;  
    while (ECanaRegs.CANTA.bit.TA1 == 0);  
    ECanaRegs.CANTA.bit.TA1 = 1; 
    m->MessageCounter = 0;
}

//============================================================================
// CAN_MailBoxRxSetAndInit - Configure mailbox for RX; Direction -> MD=1
//============================================================================

void CAN_MailBoxRxSetAndInit(CAN_RX_MESSAGE_STRUCT *m, CAN_IDTYPE Rx_MAILBOX_TYPE, uint32_t Rx_MAILBOX_ID, uint16_t Rx_MAILBOX_LENGTH, uint32_t Rx_MAILBOX_MASK)
{
    m->CAN_IdType = Rx_MAILBOX_TYPE;
    m->CanID = Rx_MAILBOX_ID;
    m->length = Rx_MAILBOX_LENGTH;
    m->CanIDMask = Rx_MAILBOX_MASK;
    m->newDataAvailable = 0;

    struct ECAN_REGS ECanaShadow;

    EALLOW; 
    
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME10 = 0;  // Disable mailbox during setup
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    
    ECanaShadow.CANMD.all = ECanaRegs.CANMD.all;
    ECanaShadow.CANMD.bit.MD10 = 1;  // Fix: Set to receive
    ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;
        
    volatile struct MBOX *p = &ECanaMboxes.MBOX10;
    if (Rx_MAILBOX_TYPE == CAN_11BIT) {
        p->MSGID.bit.IDE = 0;
        p->MSGID.bit.STDMSGID = Rx_MAILBOX_ID;  // e.g., 0x0AA
    } else {
        p->MSGID.bit.IDE = 1;
        p->MSGID.all = (Rx_MAILBOX_ID & 0x1FFFFFFF);
    }
    p->MSGCTRL.bit.DLC = Rx_MAILBOX_LENGTH;  // 8 bytes

    ECanaLAMRegs.LAM10.bit.LAMI = 0;  // Accept all messages
    ECanaLAMRegs.LAM10.bit.LAM_H = 0;
    ECanaLAMRegs.LAM10.bit.LAM_L = 0;

    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME10 = 1;  // Enable mailbox
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;

    EDIS;
}

//========================================================================
// CAN_MessageRead - If an RX mailbox has new data, transfer it into the struct
//========================================================================
void CAN_MessageRead(CAN_RX_MESSAGE_STRUCT *CanRxMessage)
{
    if (ECanaRegs.CANRMP.bit.RMP10) {  // Fix: Check if message is pending
        volatile struct MBOX *MboxPtr = &ECanaMboxes.MBOX10;
        CanRxMessage->data[0] = MboxPtr->MDL.byte.BYTE0;
        CanRxMessage->data[1] = MboxPtr->MDL.byte.BYTE1;
        CanRxMessage->data[2] = MboxPtr->MDL.byte.BYTE2;
        CanRxMessage->data[3] = MboxPtr->MDL.byte.BYTE3;
        CanRxMessage->data[4] = MboxPtr->MDH.byte.BYTE4;
        CanRxMessage->data[5] = MboxPtr->MDH.byte.BYTE5;
        CanRxMessage->data[6] = MboxPtr->MDH.byte.BYTE6;
        CanRxMessage->data[7] = MboxPtr->MDH.byte.BYTE7;

        ECanaRegs.CANRMP.bit.RMP10 = 1;  // Clear pending flag
        CanRxMessage->newDataAvailable = 1;
        CanRxMessage->MessageCounter++;
    }
}

void setupTIMER(){
    
    CpuTimer0Regs.TCR.bit.TSS =1;

    CpuTimer0Regs.TPR.bit.TDDR=0xC8;
    CpuTimer0Regs.TPRH.bit.TDDRH=0x00;

    CpuTimer0Regs.PRD.all=100000;

    CpuTimer0Regs.TCR.bit.TIE=1;
    CpuTimer0Regs.TCR.bit.TRB=1;
    CpuTimer0Regs.TCR.bit.TSS=0;

    // ---
    
    CpuTimer1Regs.TCR.bit.TSS =1;
    CpuTimer1Regs.TPR.bit.TDDR=0xC8;
    CpuTimer1Regs.TPRH.bit.TDDRH=0x00;    
    CpuTimer1Regs.PRD.all=100000;

    CpuTimer1Regs.TCR.bit.TIE=1;
    CpuTimer1Regs.TCR.bit.TRB=1;
    CpuTimer1Regs.TCR.bit.TSS=0;

}

// ###_EOF_###

