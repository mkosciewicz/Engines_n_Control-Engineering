#include "DSP28x_Project.h"  
#include "_hdrs_/tms28335_setupPWM_n_TZ.h"  

void setupPWM() {
    EALLOW;
    // ******************************
    // SO_EPWM Module 1 configuration
    // ******************************
    EPwm1Regs.TBCTR = 0x0000;
    EPwm1Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetrical mode
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = 0x3;
    EPwm1Regs.TBCTL.bit.CLKDIV = 0;
    EPwm1Regs.TBCTL.bit.FREE_SOFT = 0;

    // --- Action_Qualifier ---
    EPwm1Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;

    // --- Dead_Band ---
    EPwm1Regs.DBCTL.bit.IN_MODE = DB_DISABLE;
    EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active High complementary 
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // Enable Dead-band module
    // Dead band 2us
    EPwm1Regs.DBFED = 100; 
    EPwm1Regs.DBRED = 100; 
    
    // --- Trip_Zone ---
    EPwm1Regs.TZSEL.bit.OSHT1 = 1;
    EPwm1Regs.TZCTL.bit.TZB = 2;
    EPwm1Regs.TZCTL.bit.TZA = 2;
    EPwm1Regs.TZEINT.bit.OST = 1;

    // --- SOC_syncRefernce_phsA ---
    EPwm1Regs.TBCTL.bit.SYNCOSEL = 1;
    EPwm1Regs.ETSEL.bit.SOCAEN = 1;
    EPwm1Regs.ETSEL.bit.SOCBEN = 1;

    EPwm1Regs.ETSEL.bit.SOCASEL = 2;
    EPwm1Regs.ETSEL.bit.SOCBSEL = 2;
    
    EPwm1Regs.ETPS.bit.SOCAPRD = 1;    
    EPwm1Regs.ETPS.bit.SOCBPRD = 1;

    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Master module
    EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;

    EPwm1Regs.CMPA.half.CMPA = 0;
    // ******* EO_EPWM Module 1 configuration *******

    // ******************************
    // SO_EPWM Module 2 configuration
    // ******************************
    EPwm2Regs.TBCTR = 0x0000;
    EPwm2Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetrical mode
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = 0x3;
    EPwm2Regs.TBCTL.bit.CLKDIV = 0;
    EPwm2Regs.TBCTL.bit.FREE_SOFT = 0;

    // --- Action_Qualifier ---
    EPwm2Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;

    // --- Dead_Band ---
    EPwm2Regs.DBCTL.bit.IN_MODE = DB_DISABLE;
    EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active High complementary 
    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // Enable Dead-band module
    // Dead band 2us
    EPwm2Regs.DBFED = 100; 
    EPwm2Regs.DBRED = 100; 
    
    // --- Trip_Zone ---
    EPwm2Regs.TZSEL.bit.OSHT1 = 1;
    EPwm2Regs.TZCTL.bit.TZB = 2;
    EPwm2Regs.TZCTL.bit.TZA = 2;
    EPwm2Regs.TZEINT.bit.OST = 1;

    EPwm2Regs.TBCTL.bit.SYNCOSEL = 0;
    
    EPwm2Regs.TBCTL.bit.PHSEN = 1;
    EPwm2Regs.TBPHS.half.TBPHS = 1;
    EPwm2Regs.TBCTL.bit.PHSDIR = 1;

    EPwm2Regs.CMPA.half.CMPA = 0;
    // ******* EO_EPWM Module 2 configuration *******

    // ******************************
    // SO_EPWM Module 3 configuration
    // ******************************
    EPwm3Regs.TBCTR = 0x0000;
    EPwm3Regs.TBPRD = EPWM_TIMER_TBPRD;
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetrical mode
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0x3;
    EPwm3Regs.TBCTL.bit.CLKDIV = 0;
    EPwm3Regs.TBCTL.bit.FREE_SOFT = 0;

    // --- Action_Qualifier ---
    EPwm3Regs.AQCTLA.bit.ZRO = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.PRD = AQ_NO_ACTION;
    EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;

    // --- Dead_Band ---
    EPwm3Regs.DBCTL.bit.IN_MODE = DB_DISABLE;
    EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC; // Active High complementary 
    EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE; // Enable Dead-band module
    // Dead band 2us
    EPwm3Regs.DBFED = 100; 
    EPwm3Regs.DBRED = 100; 
    
    // --- Trip_Zone ---
    EPwm3Regs.TZSEL.bit.OSHT1 = 1;
    EPwm3Regs.TZCTL.bit.TZB = 2;
    EPwm3Regs.TZCTL.bit.TZA = 2;
    EPwm3Regs.TZEINT.bit.OST = 1;

    EPwm3Regs.TBCTL.bit.SYNCOSEL = 0;

    EPwm3Regs.TBCTL.bit.PHSEN = 1;
    EPwm3Regs.TBPHS.half.TBPHS = 1;
    EPwm3Regs.TBCTL.bit.PHSDIR = 1;

    EPwm3Regs.CMPA.half.CMPA = 0;
    // ******* EO_EPWM Module 3 configuration *******

    EDIS;
}

void PwmTripZoneControl(PWMTripCmd cmd)
{
    EALLOW;
    switch (cmd) {
    case PWM_TZ_TRIP:
        EPwm1Regs.TZFRC.bit.OST = 1;
        EPwm2Regs.TZFRC.bit.OST = 1;
        EPwm3Regs.TZFRC.bit.OST = 1;
        break;

    case PWM_TZ_RESET:
        // Clear the trip latch and interrupt flags
        EPwm1Regs.TZCLR.bit.OST = 1;
        EPwm1Regs.TZCLR.bit.INT = 1;
        EPwm2Regs.TZCLR.bit.OST = 1;
        EPwm2Regs.TZCLR.bit.INT = 1;
        EPwm3Regs.TZCLR.bit.OST = 1;
        EPwm3Regs.TZCLR.bit.INT = 1;
        break;
    }
    EDIS;
}

