#include "_hdrs_/im_modulator.h"
#include <stdbool.h>
int x;
static float Udc_buffer[10] = {0.0f};
static int Udc_index = 0;
static bool Udc_buffer_reset = true;
float Udc_sum = 0.0f;

void Modulator_init(MODULATOR_STRUCT* modulator) {
    modulator->x1 = 0;
    modulator->x2 = 0;
    modulator->x3 = 0;
    modulator->y1 = 0;
    modulator->y2 = 0;
    modulator->y3 = 0;
    modulator->min = 0;
    modulator->max = 0;
    modulator->sum = 0;
}

void Modulator_calc(MODULATOR_STRUCT* modulator, float x1, float x2, float x3, float Udc) {
    modulator->x1 = x1;
    modulator->x2 = x2;
    modulator->x3 = x3;

    float maxVal = fmaxf(fmaxf(x1, x2), x3);
    float minVal = fminf(fminf(x1, x2), x3);
    
    float zeroSeq = -0.5f * (maxVal + minVal);

    modulator->y1 = (x1 + zeroSeq) / Udc;
    modulator->y2 = (x2 + zeroSeq) / Udc;
    modulator->y3 = (x3 + zeroSeq) / Udc;

    // Modulator's offset to fit in [0, 1]
    modulator->y1 += 0.5f;
    modulator->y2 += 0.5f;
    modulator->y3 += 0.5f;


    if(modulator->y1 < 0.0f) modulator->y1 = 0.0f; else if(modulator->y1 > 1.0f) modulator->y1 = 1.0f;
    if(modulator->y2 < 0.0f) modulator->y2 = 0.0f; else if(modulator->y2 > 1.0f) modulator->y2 = 1.0f;
    if(modulator->y3 < 0.0f) modulator->y3 = 0.0f; else if(modulator->y3 > 1.0f) modulator->y3 = 1.0f;
}
