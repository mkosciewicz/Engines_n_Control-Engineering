#include "DSP28x_Project.h"
#include "_hdrs_/im_ABC_dq_ABC.h"

const float PI = 3.14159265f;

void ABC_dq_ABC_Init(ABC_DQ_ABC_STRUCT* signal) {
    signal->a_phs = 0;
    signal->b_phs = 0;
    signal->c_phs = 0;
    signal->alfa_axis = 0;
    signal->beta_axis = 0;
    signal->d_axis = 0;
    signal->q_axis = 0;
    signal->theta_electro = 0;
}

void calculateTrigonomValues(float theta, float* cos_theta, float* sin_theta, float* cos_theta_minus_2pi_3, float* sin_theta_minus_2pi_3, float* cos_theta_plus_2pi_3, float* sin_theta_plus_2pi_3) {
    *cos_theta = (float)cos(theta);
    *sin_theta = (float)sin(theta);
    *cos_theta_minus_2pi_3 = (float)cos(theta - (PI * 2.0f / 3.0f));
    *sin_theta_minus_2pi_3 = (float)sin(theta - (PI * 2.0f / 3.0f));
    *cos_theta_plus_2pi_3 = (float)cos(theta + (PI * 2.0f / 3.0f));
    *sin_theta_plus_2pi_3 = (float)sin(theta + (PI * 2.0f / 3.0f));
}

void abc2dq(ABC_DQ_ABC_STRUCT* signal, float a_phs, float b_phs, float c_phs, float theta_electro) {
    signal->a_phs = a_phs;
    signal->b_phs = b_phs;
    signal->c_phs = c_phs;
    signal->theta_electro = theta_electro;

    float cos_theta, sin_theta;
    float cos_theta_minus_2pi_3, sin_theta_minus_2pi_3;
    float cos_theta_plus_2pi_3, sin_theta_plus_2pi_3;
    calculateTrigonomValues(theta_electro, &cos_theta, &sin_theta, &cos_theta_minus_2pi_3, &sin_theta_minus_2pi_3, &cos_theta_plus_2pi_3, &sin_theta_plus_2pi_3);

    signal->d_axis = (2.0f / 3.0f) * (a_phs * cos_theta
        + b_phs * (float)cos(theta_electro - 2.0f * PI / 3.0f)
        + c_phs * (float)cos(theta_electro + 2.0f * PI / 3.0f));

    signal->q_axis = (2.0f / 3.0f) * (a_phs * (-sin_theta)
        + b_phs * (-(float)sin(theta_electro - 2.0f * PI / 3.0f))
        + c_phs * (-(float)sin(theta_electro + 2.0f * PI / 3.0f)));
}

void dq2abc(ABC_DQ_ABC_STRUCT* signal, float d_axis, float q_axis, float theta_electro) {
    signal->d_axis = d_axis;
    signal->q_axis = q_axis;
    signal->theta_electro = theta_electro;

    float cos_theta, sin_theta;
    float cos_theta_minus_2pi_3, sin_theta_minus_2pi_3;
    float cos_theta_plus_2pi_3, sin_theta_plus_2pi_3;
    calculateTrigonomValues(theta_electro, &cos_theta, &sin_theta, &cos_theta_minus_2pi_3, &sin_theta_minus_2pi_3, &cos_theta_plus_2pi_3, &sin_theta_plus_2pi_3);

    signal->a_phs = d_axis * cos_theta - q_axis * sin_theta;
    signal->b_phs = d_axis * cos_theta_minus_2pi_3 - q_axis * sin_theta_minus_2pi_3;
    signal->c_phs = d_axis * cos_theta_plus_2pi_3 - q_axis * sin_theta_plus_2pi_3;
}

void abc2alfa_beta(ABC_DQ_ABC_STRUCT* signal, float a_phs, float b_phs, float c_phs) {
    signal->a_phs = a_phs;
    signal->b_phs = b_phs;
    signal->c_phs = c_phs;

    signal->alfa_axis = (2.0f / 3.0f) * (a_phs - 0.5f * (b_phs + c_phs));
    signal->beta_axis = (2.0f / sqrtf(3.0f)) * (b_phs - c_phs);
}

void dq2ab(ABC_DQ_ABC_STRUCT* signal, float d_axis, float q_axis, float theta_electro, float scalar) {
    signal->d_axis = d_axis;
    signal->q_axis = q_axis;
    signal->theta_electro = theta_electro; // Store theta for later use in ab2abc

    // Inverse Park Transform
    float cos_theta = cosf(theta_electro);
    float sin_theta = sinf(theta_electro);

    signal->alfa_axis = scalar * (d_axis * cos_theta - q_axis * sin_theta);
    signal->beta_axis = scalar * (d_axis * sin_theta + q_axis * cos_theta);
}

void ab2abc(ABC_DQ_ABC_STRUCT* signal, float alfa_axis, float beta_axis) {
    signal->alfa_axis = alfa_axis;
    signal->beta_axis = beta_axis;

    // Inverse Clarke Transform (Power Invariant version)
    signal->a_phs = alfa_axis;
    signal->b_phs = -0.5f * alfa_axis + (sqrtf(3.0f) / 2.0f) * beta_axis;
    signal->c_phs = -0.5f * alfa_axis - (sqrtf(3.0f) / 2.0f) * beta_axis;
}
