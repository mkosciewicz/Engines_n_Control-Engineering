#include "DSP28x_Project.h"    
#include "_hdrs_/tms28335_setupADC.h"

void setupADC() {
    
    InitAdc();
    EALLOW;
    SysCtrlRegs.HISPCP.bit.HSPCLK = 1;  // Clocking from processor clock via divider
    
    /* Setup of Resolution */
    AdcRegs.ADCTRL1.bit.CPS = 0;
    AdcRegs.ADCTRL3.bit.ADCCLKPS = 2;   // 6.25 [MHz]
    AdcRegs.ADCTRL1.bit.ACQ_PS = 3;    // 3 sample and hold cycles

    AdcRegs.ADCTRL1.bit.SEQ_CASC = 0;
    AdcRegs.ADCTRL1.bit.CONT_RUN = 0;
    AdcRegs.ADCTRL1.bit.SEQ_OVRD = 0;

    // Check the /home/mkoscie/PW_EE/Bachelor/Code/Inverter/DSP_TMS320x335/Notes/_imgs_/adc_selectio.png
    
    /*******_Current_Measurments_******/
    AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0x01; // Ifb-U with dnp; where dnp means "Do Not Populate" -> check the schematic
    AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 0x09; // If a component is not populated, it means that it is not present on the board
    AdcRegs.ADCCHSELSEQ1.bit.CONV02 = 0x03; // dnp means that the component can be accepted at the indicated location, but it is not required
    
    /*******_Voltage_Measurment_******/
    AdcRegs.ADCCHSELSEQ1.bit.CONV03 = 0x07; // Udc

    AdcRegs.ADCMAXCONV.bit.MAX_CONV1 = 0x03; // 3 conversions
    AdcRegs.ADCMAXCONV.bit.MAX_CONV2 = 0x00;
    
    AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 1;  // Enable interrupt from Sequencer 1
    AdcRegs.ADCTRL2.bit.INT_MOD_SEQ1 = 0; // Generate interrupt after each sequence
    AdcRegs.ADCTRL2.bit.INT_ENA_SEQ2 = 1;  // Enable interrupt from Sequencer 2
    AdcRegs.ADCTRL2.bit.INT_MOD_SEQ2 = 0; // Generate interrupt after each sequence

    AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;
    AdcRegs.ADCTRL2.bit.EPWM_SOCB_SEQ2 = 1;

    EDIS;
}
