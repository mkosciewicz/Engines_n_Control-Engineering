#ifndef ESTIMATION_H
#define ESTIMATION_H

#include <stddef.h>
#include <math.h>

#include "GlobalVariables.h"

typedef struct {
    float Ts; 
    //---
    float L_m;         // Magnatizing inductance
    float T_r;         // Rotor time constant
    float phi_r;       // Rotor flux magnitude (constant)
    int p;           // Number of pole pairs
    float K_slip;      // Slip frequency constant
    float theta_e;      
    float theta_electro_wrapped;
} ESTIMATOR_STRUCT;


void ElectroTheta_Estimator_Init(ESTIMATOR_STRUCT* electroThetaEstimator, float Ts, float L_m, float T_r, float phi_r, int p);
float ElectroTheta_Estimator_Update(ESTIMATOR_STRUCT* electroThetaEstimator, 
    float phi_r, float i_qs_ref, float omega_m);
float calculateMaxSpeed(float VMAX, float rotorFlux);
#endif // ESTIMATION_H
