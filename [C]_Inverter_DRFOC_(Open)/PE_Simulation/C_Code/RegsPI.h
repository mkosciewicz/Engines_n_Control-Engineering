#include <math.h>
#include <stdbool.h>
#include <string.h>

typedef struct {
    float out;
    float out_prev;

    float out_raw;
    float out_raw_prev;
    
    float out_sat;
    float out_sat_prev;

    float integrator_prev;
    float integrator;
    float proportional;
    
    float kp;
    float ki;
    float Kbc;

    float error;

    float Ts;
    float min;
    float max;
    bool has_limits;
    bool BackCalc; 
} PI_STRUCT;

void Pi_Init(PI_STRUCT* Pi, float Ts, float kp, float ki, float Kbc, bool has_limits, bool bc);
void Pi_Calc(PI_STRUCT* Pi, float x_ref, float x_meas, float min, float max);


