// discrete_tranf_func.h

#ifndef DISCRETE_TRANF_FUNC_H
#define DISCRETE_TRANF_FUNC_H

#include <stddef.h>

#define MAX_ORDER 2  // Maximum order of the transfer function (adjust as needed)

typedef struct {
    float num_coeffs[MAX_ORDER + 1];   // Numerator coefficients
    float den_coeffs[MAX_ORDER + 1];   // Denominator coefficients
    size_t order_num;                  // Order of numerator
    size_t order_den;                  // Order of denominator
    float x_buffer[MAX_ORDER + 1];     // Input buffer
    float y_buffer[MAX_ORDER + 1];     // Output buffer
} DISCRETE_TRANSFER_FUNC_STRUCT;

// Function prototypes
void DiscreteTF_Init(DISCRETE_TRANSFER_FUNC_STRUCT *dtf,
                     const float *num_coeffs, size_t order_num,
                     const float *den_coeffs, size_t order_den);

float DiscreteTF_Update(DISCRETE_TRANSFER_FUNC_STRUCT *dtf, float input);

#endif // DISCRETE_TRANF_FUNC_H
