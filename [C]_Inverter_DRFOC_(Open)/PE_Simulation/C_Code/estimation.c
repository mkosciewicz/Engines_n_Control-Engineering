﻿#include "estimation.h"

extern float PI;
float doublePI = 6.283185307179586f;
extern GLOBAL_VARIABLES_STRUCT gVars;
 

void ElectroTheta_Estimator_Init(ESTIMATOR_STRUCT* electroThetaEstimator, float Ts, float L_m, float T_r, float phi_r, int p) {
    electroThetaEstimator->Ts = Ts;
    electroThetaEstimator->L_m = L_m;
    electroThetaEstimator->T_r = T_r;
    electroThetaEstimator->phi_r = phi_r;      // Rotor flux magnitude (constant)
    electroThetaEstimator->p = p;              // Number of pole pairs
    electroThetaEstimator->K_slip = 0.0f;
    electroThetaEstimator->theta_e = 0.0f;
	electroThetaEstimator->theta_electro_wrapped = 0.0f;
}

float ElectroTheta_Estimator_Update(ESTIMATOR_STRUCT* electroThetaEstimator, 
    float phi_r, float i_qs_ref, float omega_m) {

if (fabsf(electroThetaEstimator->theta_electro_wrapped) < 1e-6f &&
fabsf(electroThetaEstimator->theta_e) < 1e-6f) {
electroThetaEstimator->theta_e = -2.0944f;
electroThetaEstimator->theta_electro_wrapped = -2.0944f;
}

electroThetaEstimator->K_slip = electroThetaEstimator->L_m / 
    (phi_r > 0.01f ? phi_r : 0.01f) / 
    electroThetaEstimator->T_r; 
float omega_slip = electroThetaEstimator->K_slip * i_qs_ref;

gVars.omega_e = electroThetaEstimator->p * omega_m + omega_slip;

electroThetaEstimator->theta_e += gVars.omega_e * electroThetaEstimator->Ts;
electroThetaEstimator->theta_electro_wrapped = fmodf(electroThetaEstimator->theta_e + PI, doublePI) - PI;

return electroThetaEstimator->theta_electro_wrapped;
}

float calculateMaxSpeed(float VMAX, float rotorFlux) {
   
    float omega_m_max = (VMAX / rotorFlux) / 2.0;
    return omega_m_max;
}