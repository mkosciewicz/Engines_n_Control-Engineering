#ifndef Q_AXIS_LIMITER_H
#define Q_AXIS_LIMITER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
    MIN,MAX
} Operation;

typedef struct {
    float ref_statorVoltage_d;
    float ref_statorVoltage_q;
} LIMITER_STRUCT;

float min_max(float input1, float input2, Operation op);

void calculateLimiter(LIMITER_STRUCT* result, float currentPI_output_d, float currentPI_output_q, float max_constant);

#endif // Q_AXIS_LIMITER_H
