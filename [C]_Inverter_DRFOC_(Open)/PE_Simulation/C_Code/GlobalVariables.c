#include "GlobalVariables.h"

void GlobalVariables_Init(GLOBAL_VARIABLES_STRUCT* gVars)
{
    gVars->Ts = 0.0001f;
    gVars->V_dcLink_rated = 375.59f;
    gVars->V_dcLink = 375.59f; //64.0f;

    gVars->statorCurrent_freq = 50.0f;
    gVars->scalarDC_Link = gVars->V_dcLink / gVars->V_dcLink_rated;

    gVars->poles_pairs_nr = 2;
	gVars->Tustin = 1; gVars->FE = 2; gVars->BE = 3;

    gVars->stator_leakageInductance = 0.04f;
    gVars->rotor_leakageInductance = gVars->stator_leakageInductance;
    gVars->magnetizing_Inductance = 0.49;
	gVars->stator_Inductance = gVars->stator_leakageInductance + gVars->magnetizing_Inductance;
	gVars->rotor_Inductance = gVars->rotor_leakageInductance + gVars->magnetizing_Inductance;
    
    gVars->stator_Resistance = 20.82f;
    gVars->rotor_Resistance = 11.00f;
    
    // #######
    gVars->ref_rotorFlux_rated = 0.45104f; // 0.3185f; // Calced based on the Iphs_RMS 0.45104f;
    // #######

    gVars->J = 0.00061f;

    gVars->rotor_Tau = 0.00f;
    gVars->prefilter_Tau = 0.0f;

    gVars->KP_FLUX_CTRL = 85.0f;
    gVars->KI_FLUX_CTRL = 2000.0f;
    gVars->KB_FLUX_CTRL = 0.0f;

    gVars->KP_SPEED_CTRL = 0.0f;
    gVars->KI_SPEED_CTRL = 0.0f;
    gVars->KB_SPEED_CTRL = 0.0f;

    gVars->KP_CURR_CTRL = 0.0f;
    gVars->KI_CURR_CTRL = 0.0f;
    gVars->KB_CURR_CTRL = 0.0f;

	TransferFunctionCoeffs_Init(&gVars->fluxEstimator);
	TransferFunctionCoeffs_Init(&gVars->ref_speed);
	TransferFunctionCoeffs_Init(&gVars->meas_speed);
    TransferFunctionCoeffs_Init(&gVars->voltage_lpf);
    gVars->I_MAX = 1.73f;
    gVars->q_axis_limit = 1.0f;

    gVars->V_dcLink_scalar = 0.577350269f;
    gVars->V_dcLink_minLimit = 1e9f;
    gVars->V_dcLink_maxLimit = 1e9f;
    gVars->modulator_scalar = 1.0f;

    gVars->offset = 0.0001f;
    gVars->meas_statorCurrent_a = 0.0f;
    gVars->meas_statorCurrent_b = 0.0f;
    gVars->meas_statorCurrent_c = 0.0f;
    gVars->meas_statorCurrent_d = 0.0f;
    gVars->meas_statorCurrent_q = 0.0f;

    gVars->ref_statorCurrent_d = 0.0f;
    gVars->ref_statorCurrent_q = 0.0f;
    gVars->ref_statorCurrent_d_nonzero = 0.0f;

    gVars->ref_statorVoltage_d = 0.0f;
    gVars->ref_statorVoltage_q = 0.0f;
    gVars->ref_statorVoltage = 0.0f;
    gVars->request_DC_Link_Vltg = 0.0f;
    
    gVars->estima_rotorFlux = 0.0f;
    gVars->omega_e = 0.0f;
    gVars->estima_theta_electro = 0.0f;

    gVars->set_rotorSpeed = 0.0f;
    gVars->ref_rotorSpeed = 0.0f;
    gVars->meas_rotorSpeed = 0.0f;
    
}

void TransferFunctionCoeffs_Init(TRANSFER_FUNC_COEFFS_STRUCT* tf)
{
    tf->num_coeffs[0] = 0.0f;
    tf->den_coeffs[0] = 0.0f;
    tf->den_coeffs[1] = 0.0f;
    tf->order_num = 0;
    tf->order_den = 1;
}

/*========================
=    Regs_Gains Calcs    =
==========================*/
void Parameters_Calc(GLOBAL_VARIABLES_STRUCT* gVars)
{
    /* -------------------------
       Fixed or derived values
       ------------------------- */
       float delays_Tau            = 1.5f * gVars->Ts;  
       float k_converter           = 1.0f; 
       float LPF_gain              = 15.0f;
       float tau_measurement_omega = LPF_gain * gVars->Ts; 
       float k_measurement_omega   = 1.0f;       
       float c_viscosity           = 0.01f;     
       float alpha                 = 2.0f;

    gVars->rotor_Tau = gVars->rotor_Inductance / gVars->rotor_Resistance;

    /* -------------------------
       1) Current Controller
       ------------------------- */
    float R_MO           = gVars->stator_Resistance + gVars->rotor_Resistance;
    float L_MO           = gVars->stator_leakageInductance + gVars->rotor_leakageInductance;
    float tau_MO_dominant= L_MO / R_MO;
    float tau_MO_sum_small   = delays_Tau;
    float k_plant_MO     = k_converter / R_MO;

    float K_reg_curr_MO  = 0.5f /(k_plant_MO * tau_MO_sum_small);

    gVars->KP_CURR_CTRL = K_reg_curr_MO * tau_MO_dominant;
    gVars->KI_CURR_CTRL = K_reg_curr_MO;
    gVars->KB_CURR_CTRL = gVars->KI_CURR_CTRL / gVars->KP_CURR_CTRL;

    /* -------------------------
       2) Speed Controller
       ------------------------- */
    float k_plant_SO = (1.5f * gVars->magnetizing_Inductance / gVars->rotor_Inductance)
                       * gVars->poles_pairs_nr
                       * gVars->ref_rotorFlux_rated
                       * (1.0f/c_viscosity) 
                       * k_measurement_omega;

    float tau_SO_dominant  = gVars->J / c_viscosity;
    float tau_SO_sum_small = 2.0f * tau_MO_sum_small + tau_measurement_omega;

    float T_reg_omega_SO = alpha * alpha * tau_SO_sum_small;
    float K_reg_omega_SO = tau_SO_dominant
                           / (alpha * alpha * alpha
                              * k_plant_SO
                              * tau_SO_sum_small
                              * tau_SO_sum_small);

    gVars->KP_SPEED_CTRL = K_reg_omega_SO * T_reg_omega_SO;
    gVars->KI_SPEED_CTRL = K_reg_omega_SO;
    gVars->KB_SPEED_CTRL = K_reg_omega_SO / gVars->KP_SPEED_CTRL;
	gVars->KB_FLUX_CTRL = gVars->KI_FLUX_CTRL / gVars->KP_FLUX_CTRL;

    gVars->prefilter_Tau = ((gVars->KP_SPEED_CTRL) / (gVars->KI_SPEED_CTRL)) * LPF_gain;
    
	TranferFunctionCoeffs_Calc(gVars->magnetizing_Inductance, gVars->rotor_Tau, 1.0f, gVars->Ts, gVars->Tustin, gVars->fluxEstimator.num_coeffs, gVars->fluxEstimator.den_coeffs, &gVars->fluxEstimator.order_num, &gVars->fluxEstimator.order_den);
    TranferFunctionCoeffs_Calc(1.0f, gVars->prefilter_Tau, 1.0f, gVars->Ts, gVars->Tustin, gVars->ref_speed.num_coeffs, gVars->ref_speed.den_coeffs, &gVars->ref_speed.order_num, &gVars->ref_speed.order_den);
    TranferFunctionCoeffs_Calc(1.0f, tau_measurement_omega, 1.0f, gVars->Ts, gVars->Tustin, gVars->meas_speed.num_coeffs, gVars->meas_speed.den_coeffs, &gVars->meas_speed.order_num, &gVars->meas_speed.order_den);
    TranferFunctionCoeffs_Calc(1.0f, tau_measurement_omega, 1.0f, gVars->Ts, gVars->Tustin, gVars->voltage_lpf.num_coeffs, gVars->voltage_lpf.den_coeffs, &gVars->voltage_lpf.order_num, &gVars->voltage_lpf.order_den);
}

void TranferFunctionCoeffs_Calc(
   float cNum,    
    float cDen1,   // first denominator coefficient (s^1)
    float cDen2,   
    float Ts,
    int   method,
    float *out_num_coeffs,  // array to hold discrete numerator
    float *out_den_coeffs,  
    size_t *out_order_num,  
    size_t *out_order_den   
) {    

    ContinuousTF ctf;
    ctf.num_coeffs[0] = cNum;
    ctf.den_coeffs[0] = cDen1;
    ctf.den_coeffs[1] = cDen2;
    ctf.order_num     = 0; 
    ctf.order_den     = 1; 

    DiscreteTF localDTF;
    discretizeTF(&ctf, &localDTF, Ts, method);

    // Now copy results into user-provided arrays
    out_num_coeffs[0] = localDTF.num_coeffs[0];
    out_num_coeffs[1] = localDTF.num_coeffs[1];

    out_den_coeffs[0] = localDTF.den_coeffs[0];
    out_den_coeffs[1] = localDTF.den_coeffs[1];

    // Return discrete orders
    *out_order_num = (size_t)localDTF.order_num;
    *out_order_den = (size_t)localDTF.order_den;
}

void discretizeTF(ContinuousTF* ctf, DiscreteTF* dtf, float Ts, int method)
{
    float s_sub_num[DISCRETE_ORDER + 1] = {0.0f};
    float s_sub_den[DISCRETE_ORDER + 1] = {0.0f};

    // Initialize arrays in DiscreteTF
    for (int i = 0; i <= DISCRETE_ORDER; i++) {
        dtf->num_coeffs[i] = 0.0f;
        dtf->den_coeffs[i] = 0.0f;
    }

    // Set up s-substitution arrays based on method
    float s_num[2], s_den[2];
    if (method == 1) {  // Tustin
        s_num[0] =  2.0f / Ts;
        s_num[1] = -2.0f / Ts;
        s_den[0] =  1.0f;
        s_den[1] =  1.0f;
    } else if (method == 2) {  // Forward Euler
        s_num[0] =  1.0f / Ts;
        s_num[1] = -1.0f / Ts;
        s_den[0] =  0.0f;
        s_den[1] =  1.0f;
    } else if (method == 3) {  // Backward Euler
        s_num[0] =  1.0f / Ts;
        s_num[1] = -1.0f / Ts;
        s_den[0] =  1.0f;
        s_den[1] =  0.0f;
    } else {
        exit(1);
    }

    // Substitute s in the numerator
    for (int i = 0; i <= ctf->order_num; i++) {
        int   power = ctf->order_num - i;
        float coeff = ctf->num_coeffs[i];

        float temp_num[DISCRETE_ORDER + 1] = {0.0f};
        temp_num[0] = 1.0f;

        // Expand (s_num)^power
        for (int p = 0; p < power; p++) {
            float new_temp_num[DISCRETE_ORDER + 1] = {0.0f};
            for (int j = 0; j <= DISCRETE_ORDER; j++) {
                if (temp_num[j] != 0.0f) {
                    for (int k = 0; k <= 1; k++) {
                        int idx = j + k;
                        if (idx <= DISCRETE_ORDER) {
                            new_temp_num[idx] += temp_num[j] * s_num[k];
                        }
                    }
                }
            }
            for (int j = 0; j <= DISCRETE_ORDER; j++) {
                temp_num[j] = new_temp_num[j];
            }
        }
        // Accumulate
        for (int j = 0; j <= DISCRETE_ORDER; j++) {
            s_sub_num[j] += temp_num[j] * coeff;
        }
    }

    // Substitute s in the denominator
    for (int i = 0; i <= ctf->order_den; i++) {
        int   power = ctf->order_den - i;
        float coeff = ctf->den_coeffs[i];

        float temp_den[DISCRETE_ORDER + 1] = {0.0f};
        temp_den[0] = 1.0f;

        for (int p = 0; p < power; p++) {
            float new_temp_den[DISCRETE_ORDER + 1] = {0.0f};
            for (int j = 0; j <= DISCRETE_ORDER; j++) {
                if (temp_den[j] != 0.0f) {
                    for (int k = 0; k <= 1; k++) {
                        int idx = j + k;
                        if (idx <= DISCRETE_ORDER) {
                            new_temp_den[idx] += temp_den[j] * s_num[k];
                        }
                    }
                }
            }
            for (int j = 0; j <= DISCRETE_ORDER; j++) {
                temp_den[j] = new_temp_den[j];
            }
        }
        for (int j = 0; j <= DISCRETE_ORDER; j++) {
            s_sub_den[j] += temp_den[j] * coeff;
        }
    }

    // Normalize so that denominator is monic
    float den_leading_coeff = s_sub_den[0];
    for (int i = 0; i <= DISCRETE_ORDER; i++) {
        dtf->num_coeffs[i] = s_sub_num[i] / den_leading_coeff;
        dtf->den_coeffs[i] = s_sub_den[i] / den_leading_coeff;
    }

    dtf->order_num = DISCRETE_ORDER;
    dtf->order_den = DISCRETE_ORDER;
}