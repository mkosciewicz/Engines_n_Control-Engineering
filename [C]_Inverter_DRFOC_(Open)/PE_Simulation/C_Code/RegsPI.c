#include "RegsPI.h"

void Pi_Init(PI_STRUCT* Pi, float Ts, float kp, float ki, float Kbc, bool has_limits, bool bc) {
    
    Pi->out = 0.0f;
    Pi->out_prev = 0.0f;
    Pi->out_raw = 0.0;
    Pi->out_raw_prev = 0.0f;
    Pi->out_sat = 0.0f;
    Pi->out_sat_prev = 0.0f;

    Pi->integrator_prev = 0.0f;
    Pi->integrator = 0.0f;
    Pi->proportional = 0.0f;

    Pi->kp = kp;
    Pi->ki = ki;
    Pi->Kbc = Kbc;
    Pi->error = 0.0f;
    Pi->Ts = Ts;

    Pi->has_limits = has_limits;
    Pi->BackCalc = bc;
}

void Pi_Calc(PI_STRUCT* Pi, float x_ref, float x_meas, float min, float max)
{
    Pi->min = min;
    Pi->max = max;
    
    Pi->error = x_ref - x_meas;
    Pi->proportional = Pi->kp * Pi->error;
    
    float out_raw = Pi->proportional + Pi->integrator;
    
    float out_sat;
    if (out_raw > Pi->max)
        out_sat = Pi->max;
    else if (out_raw < Pi->min)
        out_sat = Pi->min;
    else
        out_sat = out_raw;
    
    bool integrate = true;
    if (Pi->has_limits) {
        if ((out_raw > Pi->max && Pi->error > 0) ||
            (out_raw < Pi->min && Pi->error < 0))
        {
            integrate = false;
        }
    }
    
    float integrator_update = Pi->ki * Pi->error * Pi->Ts;
    
    if (Pi->BackCalc)
    {
        integrator_update += Pi->Kbc * (out_sat - out_raw) * Pi->Ts;
    }
    
    if (integrate)
        Pi->integrator += integrator_update;
    
    Pi->out_raw = out_raw;
    Pi->out_sat = out_sat;
    Pi->out = out_sat;
}

