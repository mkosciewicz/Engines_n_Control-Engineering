#include <math.h>

typedef struct  {
    float x1;
    float x2;
    float x3;
    float y1;
    float y2;
    float y3;
    float min;
    float max;
    float sum;
} MODULATOR_STRUCT;

void Modulator_init(MODULATOR_STRUCT* modulator);
void Modulator_calc(MODULATOR_STRUCT* modulator, float x1, float x2, float x3, float Udc);

