#include "DllHeader.h"
#include "GlobalVariables.h"
#include "ABC_dq_ABC.h"
#include "discrete_tranf_func.h"
#include "RegsPI.h"
#include "estimation.h"
#include "q_axis_limiter.h"
#include "modulator.h"
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
float elapsedTime = 0.0f;
int CloseLoop = 1;
float alpha = 0.0f;
float amplitude = 1.15f;
float TWO_PI = 6.283185307179586f;
float integral_d = 0.0f;
float integral_q = 0.0f;

DLLEXPORT void plecsSetSizes(struct SimulationSizes* aSizes)
{
    aSizes->numInputs = 7;
    aSizes->numOutputs = 13;
    aSizes->numStates = 0;
    aSizes->numParameters = 0;
}

/*##########_SO_INSTATIATION_#########*/
GLOBAL_VARIABLES_STRUCT gVars;
ABC_DQ_ABC_STRUCT measStatorCurrent, refStatorVoltage_alfabeta, refStatorVoltage_abc;
DISCRETE_TRANSFER_FUNC_STRUCT estimator_rotorFlux, ref_speed_tranFunc, meas_speed_tranFunc, voltage_lpf;
ESTIMATOR_STRUCT estimator_electricAngle;
PI_STRUCT Pi_id, Pi_iq, Pi_speed, Pi_flux;
LIMITER_STRUCT result;
MODULATOR_STRUCT modulator;

/*##########_EO_INSTATIATION_#########*/

DLLEXPORT void plecsStart()
{
    GlobalVariables_Init(&gVars);
    Parameters_Calc(&gVars);
    
	DiscreteTF_Init(&estimator_rotorFlux, gVars.fluxEstimator.num_coeffs, 0, gVars.fluxEstimator.den_coeffs, 1);
	DiscreteTF_Init(&ref_speed_tranFunc, gVars.ref_speed.num_coeffs, 0, gVars.ref_speed.den_coeffs, 1);
	DiscreteTF_Init(&meas_speed_tranFunc, gVars.meas_speed.num_coeffs, 0, gVars.meas_speed.den_coeffs, 1);
    DiscreteTF_Init(&voltage_lpf, gVars.voltage_lpf.num_coeffs, 0, gVars.voltage_lpf.den_coeffs, 1);
    
    ElectroTheta_Estimator_Init(&estimator_electricAngle, gVars.Ts, gVars.magnetizing_Inductance, gVars.rotor_Tau, gVars.ref_rotorFlux_rated, gVars.poles_pairs_nr);
   
    Pi_Init(&Pi_flux, gVars.Ts, gVars.KP_FLUX_CTRL, gVars.KI_FLUX_CTRL, gVars.KB_FLUX_CTRL, true, true);
    Pi_Init(&Pi_speed, gVars.Ts, gVars.KP_SPEED_CTRL, gVars.KI_SPEED_CTRL, gVars.KB_SPEED_CTRL, true, true);

    ABC_dq_ABC_Init(&measStatorCurrent);

    Pi_Init(&Pi_id, gVars.Ts, gVars.KP_CURR_CTRL, gVars.KI_CURR_CTRL, gVars.KB_CURR_CTRL, true, true);
    Pi_Init(&Pi_iq, gVars.Ts, gVars.KP_CURR_CTRL, gVars.KI_CURR_CTRL, gVars.KB_CURR_CTRL, true, true);

    ABC_dq_ABC_Init(&refStatorVoltage_abc);

	Modulator_init(&modulator);
}

DLLEXPORT void plecsOutput(struct SimulationState* aState)
{
    elapsedTime += 0.0001f;

    //%%%%%_SO_Speed PI Regulator_%%%%%  
    gVars.meas_rotorSpeed = DiscreteTF_Update(&meas_speed_tranFunc, (float)aState->inputs[0]);
    gVars.ref_rotorSpeed = DiscreteTF_Update(&ref_speed_tranFunc, (float)aState->inputs[1]);
    aState->outputs[0] = gVars.ref_rotorSpeed;
    //---   
    Pi_Calc(&Pi_speed, gVars.ref_rotorSpeed, gVars.meas_rotorSpeed, -gVars.I_MAX, gVars.I_MAX);
    gVars.ref_statorCurrent_q = Pi_speed.out;
    //---
    aState->outputs[1] = gVars.ref_statorCurrent_q;
    //---
    //%%%%%_EO_Speed PI Regulator_%%%%%   

    //%%%%%_SO_Flux_PI_n_Estimator%%%%%    
    float meas_statorCurrent_d = (gVars.ref_statorCurrent_d - Pi_id.error);
    gVars.estima_rotorFlux = DiscreteTF_Update(&estimator_rotorFlux, meas_statorCurrent_d);

    float meas_statorCurrent_q = (gVars.ref_statorCurrent_q - Pi_iq.error);
    gVars.estima_theta_electro = ElectroTheta_Estimator_Update(&estimator_electricAngle, gVars.estima_rotorFlux, meas_statorCurrent_q, gVars.meas_rotorSpeed);

    Pi_Calc(&Pi_flux, gVars.ref_rotorFlux_rated, gVars.estima_rotorFlux, -gVars.I_MAX, gVars.I_MAX);
    gVars.ref_statorCurrent_d = Pi_flux.out;

    //---
    aState->outputs[2] = gVars.ref_rotorFlux_rated;
    aState->outputs[3] = gVars.ref_statorCurrent_d;
    aState->outputs[4] = gVars.estima_rotorFlux;
    aState->outputs[5] = gVars.estima_theta_electro;
    //%%%%%_EO_Flux_PI_n_Estimator%%%%%

    //%%%%%_SO_Current Controllers_%%%%%
    gVars.meas_statorCurrent_a = (float)aState->inputs[2];
    gVars.meas_statorCurrent_b = (float)aState->inputs[3];
    gVars.meas_statorCurrent_c = (float)aState->inputs[4];
    gVars.V_dcLink = (float)aState->inputs[5];
    gVars.V_dcLink_minLimit = -gVars.V_dcLink * gVars.V_dcLink_scalar; // V_dcLink_scalar = 0.577350269f;
    gVars.V_dcLink_maxLimit = gVars.V_dcLink * gVars.V_dcLink_scalar;

    abc2dq(&measStatorCurrent, gVars.meas_statorCurrent_a, gVars.meas_statorCurrent_b, gVars.meas_statorCurrent_c, gVars.estima_theta_electro);

    aState->outputs[6] = measStatorCurrent.d_axis;    
    aState->outputs[7] = measStatorCurrent.q_axis;

    Pi_Calc(&Pi_id, gVars.ref_statorCurrent_d, measStatorCurrent.d_axis, gVars.V_dcLink_minLimit, gVars.V_dcLink_maxLimit);    
    Pi_Calc(&Pi_iq, gVars.ref_statorCurrent_q, measStatorCurrent.q_axis, gVars.V_dcLink_minLimit, gVars.V_dcLink_maxLimit);
    
    calculateLimiter(&result, Pi_id.out, Pi_iq.out, gVars.V_dcLink_maxLimit);
    gVars.ref_statorVoltage_d = result.ref_statorVoltage_d;
    gVars.ref_statorVoltage_q = result.ref_statorVoltage_q;

    aState->outputs[8] = result.ref_statorVoltage_d;
    aState->outputs[9] = result.ref_statorVoltage_q;
    //%%%%%_EO_Current Controllers_%%%%%

	// %%%%%_SO_SVPWM_%%%%%
    dq2abc(&refStatorVoltage_abc, result.ref_statorVoltage_d, result.ref_statorVoltage_q, gVars.estima_theta_electro);

    if (aState->inputs[6]) {
        Modulator_calc(&modulator, refStatorVoltage_abc.a_phs, refStatorVoltage_abc.b_phs, refStatorVoltage_abc.c_phs, gVars.V_dcLink_maxLimit);
    } else {       
        alpha = alpha + TWO_PI * gVars.statorCurrent_freq * 0.0001f;
        if (alpha > TWO_PI) alpha -= TWO_PI; 

        float phs_a = amplitude * sinf(alpha);
        float phs_b = amplitude * sinf(alpha - 2.094395102f);
        float phs_c = amplitude * sinf(alpha + 2.094395102f);

        Modulator_calc(&modulator, phs_a, phs_b, phs_c, 1.0f);
    }

    aState->outputs[10] = modulator.y1;
    aState->outputs[11] = modulator.y2;
    aState->outputs[12] = modulator.y3;    
}
