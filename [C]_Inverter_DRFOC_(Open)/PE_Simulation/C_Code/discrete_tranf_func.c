#include "discrete_tranf_func.h"

void DiscreteTF_Init(DISCRETE_TRANSFER_FUNC_STRUCT *dtf,
                     const float *num_coeffs, size_t order_num,
                     const float *den_coeffs, size_t order_den) {
    dtf->order_num = order_num;
    dtf->order_den = order_den;

    // Copy numerator coefficients
    for (size_t i = 0; i <= order_num; i++) {
        dtf->num_coeffs[i] = num_coeffs[i];
    }

    // Copy denominator coefficients
    for (size_t i = 0; i <= order_den; i++) {
        dtf->den_coeffs[i] = den_coeffs[i];
    }

    // Initialize buffers to zero
    for (size_t i = 0; i <= MAX_ORDER; i++) {
        dtf->x_buffer[i] = 0.0f;
        dtf->y_buffer[i] = 0.0f;
    }
}

float DiscreteTF_Update(DISCRETE_TRANSFER_FUNC_STRUCT *dtf, float input) {
    // Shift x_buffer (inputs)
    for (size_t i = dtf->order_num; i > 0; i--) {
        dtf->x_buffer[i] = dtf->x_buffer[i - 1];
    }
    dtf->x_buffer[0] = input;

    // Compute numerator
    float numerator = 0.0f;
    for (size_t i = 0; i <= dtf->order_num; i++) {
        numerator += dtf->num_coeffs[i] * dtf->x_buffer[i];
    }

    // Compute denominator
    float denominator = 0.0f;
    for (size_t i = 1; i <= dtf->order_den; i++) {
        denominator += dtf->den_coeffs[i] * dtf->y_buffer[i - 1];
    }

    // Compute output
    float output = (numerator - denominator) / dtf->den_coeffs[0];

    // Shift y_buffer (outputs)
    for (size_t i = dtf->order_den; i > 0; i--) {
        dtf->y_buffer[i] = dtf->y_buffer[i - 1];
    }
    dtf->y_buffer[0] = output;

    return output;
}
