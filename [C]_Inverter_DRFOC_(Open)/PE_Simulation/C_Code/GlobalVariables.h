#ifndef GLOBAL_VARIABLES_STRUCT_H
#define GLOBAL_VARIABLES_STRUCT_H

#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#define DISCRETE_ORDER 1

typedef struct {
    float num_coeffs[1];  // Because order_num = 0
    float den_coeffs[2];  // Because order_den = 1
    int   order_num;
    int   order_den;
} ContinuousTF;

typedef struct {
    float num_coeffs[DISCRETE_ORDER + 1];
    float den_coeffs[DISCRETE_ORDER + 1];
    int   order_num;
    int   order_den;
} DiscreteTF;

typedef struct {
    float num_coeffs[1];
    float den_coeffs[2];
    size_t order_num;
    size_t order_den;
} TRANSFER_FUNC_COEFFS_STRUCT;

typedef struct {
    float Ts;
    float V_dcLink_rated;
    float V_dcLink;

    float statorCurrent_freq;
    float scalarDC_Link;

    int poles_pairs_nr;
    int Tustin, FE, BE;
    float stator_leakageInductance;
    float rotor_leakageInductance;
    float magnetizing_Inductance;
    float stator_Inductance;
    float rotor_Inductance;
    float sigma;

    float rotor_Resistance;
    float stator_Resistance;

    float ref_rotorFlux_rated;
    float ref_rotorSpeed_rated;
    float J;

    float rotor_Tau;
    float prefilter_Tau;

        // PI Gains 
    float KP_FLUX_CTRL;
    float KI_FLUX_CTRL;
    float KB_FLUX_CTRL;

    float KP_SPEED_CTRL;
    float KI_SPEED_CTRL;
    float KB_SPEED_CTRL;

    float KP_CURR_CTRL;
    float KI_CURR_CTRL;
    float KB_CURR_CTRL;

	TRANSFER_FUNC_COEFFS_STRUCT fluxEstimator;
	TRANSFER_FUNC_COEFFS_STRUCT ref_speed;
	TRANSFER_FUNC_COEFFS_STRUCT meas_speed;
    TRANSFER_FUNC_COEFFS_STRUCT voltage_lpf;
    float I_MAX;
    float q_axis_limit;

    float V_dcLink_scalar;
    float V_dcLink_minLimit;
    float V_dcLink_maxLimit;
    float modulator_scalar;

    float offset;
    float meas_statorCurrent_a;
    float meas_statorCurrent_b;
    float meas_statorCurrent_c;
    float meas_statorCurrent_d;
    float meas_statorCurrent_q;

    float ref_statorCurrent_d;
    float ref_statorCurrent_d_nonzero;
    float ref_statorCurrent_q;
    float ref_statorVoltage_d;
    float ref_statorVoltage_q;
    float ref_statorVoltage;

    float request_DC_Link_Vltg;

    float estima_rotorFlux;
    float omega_e;
    float estima_theta_electro;
    bool compensa_refSpeed;

    float set_rotorSpeed;
    float ref_rotorSpeed;
    float meas_rotorSpeed;
    
} GLOBAL_VARIABLES_STRUCT;

void GlobalVariables_Init(GLOBAL_VARIABLES_STRUCT* gVars);
void TransferFunctionCoeffs_Init(TRANSFER_FUNC_COEFFS_STRUCT* tfCoeffs);
void Parameters_Calc(GLOBAL_VARIABLES_STRUCT* gVars);
void discretizeTF(ContinuousTF* ctf, DiscreteTF* dtf, float Ts, int method);
void TranferFunctionCoeffs_Calc(
   float cNum,    
    float cDen1,   // first denominator coefficient (s^1)
    float cDen2,   
    float Ts,
    int   method,
    float *out_num_coeffs,  // array to hold discrete numerator
    float *out_den_coeffs,  
    size_t *out_order_num,  
    size_t *out_order_den   
);

#endif /* GLOBAL_VARIABLES_STRUCT_H */
