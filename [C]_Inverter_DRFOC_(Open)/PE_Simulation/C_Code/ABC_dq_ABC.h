#ifndef ABC_DQ_ABC_H
#define ABC_DQ_ABC_H

#include <math.h>
#include <stddef.h>

typedef struct {
    float a_phs;
    float b_phs;
    float c_phs;
    float alfa_axis;
    float beta_axis;
    float d_axis;
    float q_axis;
    float theta_electro;
} ABC_DQ_ABC_STRUCT;

void ABC_dq_ABC_Init(ABC_DQ_ABC_STRUCT *signal);

void abc2dq(ABC_DQ_ABC_STRUCT *signal, float a_phs, float b_phs, float c_phs, float theta_electro);

void dq2abc(ABC_DQ_ABC_STRUCT *signal, float d_axis, float q_axis, float theta_electro);

void abc2alfa_beta(ABC_DQ_ABC_STRUCT* signal, float a_phs, float b_phs, float c_phs);

void dq2ab(ABC_DQ_ABC_STRUCT* signal, float d_axis, float q_axis, float theta_electro, float scalar);

void ab2abc(ABC_DQ_ABC_STRUCT* signal, float alfa_axis, float beta_axis);
#endif // ABC_DQ_ABC_H
