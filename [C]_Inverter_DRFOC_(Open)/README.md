# Overview
## [Date: 2025-02-28]

## Changes Made
- [DSP] Deleted the calling of the PWM_TZ in while.
- [DSP] Protection S.C:PASS

## Notes
- [DSP] The issue in the sequence of calling the estimators parametere is supposed to be a probing, that at each MCU or math model can differ; no literature for this.

## Next Steps
- [Report] Write it!
- [CAN] The control is possible thanks to the compensation and established communication

## Results
 - [Simul] The rotor's speed is kept stable when the Speed PI gets saturated
 - [DSP] Simulation S.C.: <span style="color:green">PASS</span>
 - [Promotor Confirmation] Closed Loop Test - S.C.: <span style="color:green">PASS</span>
 - [DSP] Inverter -> Interleaved: Voltage Request - S.C.: <span style="color:green">PASS</span>

---
 ## Review Update: [Date: 2025-02-28]

